/**
* @file     menus.cpp
* @brief    Implementacao das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    05/06/2017
*/

#include "menus.h"

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_principal ()
{
    int menu_principal;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                   Menu Principal                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Comerciante                               *" << endl;
    cout << "*      2) Cliente                                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Sair                                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao que condiz com seu tipo de usuario:" << endl << "--> ";
    cin >> menu_principal;
    cout << endl;

    return menu_principal;
}

/**
* @brief Função que imprime o menu do comerciante com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_comerciante ()
{
    int menu_comerciante;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                  Menu Comerciante                 *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Cadastrar Produto                         *" << endl;
    cout << "*      2) Remover Produto                           *" << endl;
    cout << "*                                                   *" << endl;    
    cout << "*      3) Cadastrar Fornecedor                      *" << endl;
    cout << "*      4) Remover Fornecedor                        *" << endl;
    cout << "*                                                   *" << endl;    
    cout << "*      5) Atualizar Dados de um Produto             *" << endl;
    cout << "*      6) Consulta de Produtos e Fornecedores       *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao desejada:" << endl << "--> ";
    cin >> menu_comerciante;
    cout << endl;

    return menu_comerciante;
}

/**
* @brief Função que imprime o menu de cadastramento de um produto com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_cadastramento ()
{
    int menu_cadastro;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*              Cadastramento de Produtos            *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Bebida                                    *" << endl;
    cout << "*      2) Fruta                                     *" << endl;
    cout << "*      3) Salgado                                   *" << endl;
    cout << "*      4) Doce                                      *" << endl;
    cout << "*      5) CD                                        *" << endl;
    cout << "*      6) DVD                                       *" << endl;
    cout << "*      7) Livro                                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do tipo de produto que deseja cadastrar:" << endl << "--> ";
    cin >> menu_cadastro;
    cout << endl;

    return menu_cadastro;
}

/**
* @brief Função que imprime o menu de alteracao de dados de uma bebida com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_bebida ()
{
    int menu_alteracao_bebida;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Dados Bebida                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Teor Alcoolico                            *" << endl;
    cout << "*      7) Quantidade de Acucar                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_bebida;
    cout << endl;

    return menu_alteracao_bebida;
}

/**
* @brief Função que imprime o menu de alteracao de dados de uma fruta com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_fruta ()
{
    int menu_alteracao_fruta;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Dados Fruta                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Numero do Lote                            *" << endl;
    cout << "*      7) Data de producao do lote                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_fruta;
    cout << endl;

    return menu_alteracao_fruta;
}

/**
* @brief Função que imprime o menu de alteracao de dados de um salgado com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_salgado ()
{
    int menu_alteracao_salgado;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                   Dados Salgado                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Quantidade de Sodio                       *" << endl;
    cout << "*      7) Composicao                                *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_salgado;
    cout << endl;

    return menu_alteracao_salgado;
}

/**
* @brief Função que imprime o menu de alteracao de dados de um doce com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_doce ()
{
    int menu_alteracao_doce;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Dados Doce                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Quantidade de Acucar                      *" << endl;
    cout << "*      7) Composicao                                *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_doce;
    cout << endl;

    return menu_alteracao_doce;
}

/**
* @brief Função que imprime o menu de alteracao de dados de um cd com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_cd ()
{
    int menu_alteracao_cd;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                      Dados CD                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Estilo                                    *" << endl;
    cout << "*      7) Artista                                   *" << endl;
    cout << "*      8) Album                                     *" << endl;    
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_cd;
    cout << endl;

    return menu_alteracao_cd;
}

/**
* @brief Função que imprime o menu de alteracao de dados de um dvd com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_dvd ()
{
    int menu_alteracao_dvd;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Dados DVD                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Titulo                                    *" << endl;
    cout << "*      7) Genero                                    *" << endl;
    cout << "*      8) Duracao                                   *" << endl;    
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_dvd;
    cout << endl;

    return menu_alteracao_dvd;
}

/**
* @brief Função que imprime o menu de alteracao de dados de um livro com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_livro ()
{
    int menu_alteracao_livro;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Dados Livro                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Nome                                      *" << endl;
    cout << "*      2) Codigo                                    *" << endl;
    cout << "*      3) Descricao                                 *" << endl;
    cout << "*      4) Preco                                     *" << endl;
    cout << "*      5) Quantidade em Estoque                     *" << endl;
    cout << "*      6) Titulo                                    *" << endl;
    cout << "*      7) Autor                                     *" << endl;
    cout << "*      8) Editora                                   *" << endl;
    cout << "*      9) Ano de Publicacao                         *" << endl;    
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do dado que deseja alterar:" << endl << "--> ";
    cin >> menu_alteracao_livro;
    cout << endl;

    return menu_alteracao_livro;
}

/**
* @brief Função que imprime o menu de consulta do cliente com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_cliente ()
{
    int menu_consulta_cliente;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Consultas                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Mostrar Produto por Nome                  *" << endl;
    cout << "*      2) Mostrar Produtos por Tipo                 *" << endl;
    cout << "*      3) Mostrar Todos Produtos                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção de consulta desejada:" << endl << "--> ";
    cin >> menu_consulta_cliente;
    cout << endl;

    return menu_consulta_cliente;
}

/**
* @brief Função que imprime o menu de consulta por tipo com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_tipo ()
{
    int menu_consulta_tipo;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                Tipos para Consulta                *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Bebida                                    *" << endl;
    cout << "*      2) Fruta                                     *" << endl;
    cout << "*      3) Salgado                                   *" << endl;
    cout << "*      4) Doce                                      *" << endl;
    cout << "*      5) CD                                        *" << endl;
    cout << "*      6) DVD                                       *" << endl;
    cout << "*      7) Livro                                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção do tipo de produto que deseja consultar:" << endl << "--> ";
    cin >> menu_consulta_tipo;
    cout << endl;

    return menu_consulta_tipo;
}

/**
* @brief Função que imprime o menu de consulta do comerciante com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_comerciante ()
{
    int menu_consulta_comerciante;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Consultas                     *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Mostrar Produto por Nome                  *" << endl;
    cout << "*      2) Mostrar Produtos por Tipo                 *" << endl;
    cout << "*      3) Mostrar Todos Produtos                    *" << endl;
    cout << "*      4) Mostrar Produtos por Fornecedor           *" << endl;
    cout << "*                                                   *" << endl;    
    cout << "*      5) Mostrar Todos Fornecedores                *" << endl;
    cout << "*      6) Mostrar Fornecedor por Nome               *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção de consulta desejada:" << endl << "--> ";
    cin >> menu_consulta_comerciante;
    cout << endl;

    return menu_consulta_comerciante;
}

/**
* @brief Função que imprime o menu do carrinho de compras com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_carrinho () {
    int menu_carrinho;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                Carrinho de compras                *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Ver produtos                              *" << endl;
    cout << "*      2) Inserir produtos                          *" << endl;
    cout << "*      3) Remover Produtos                          *" << endl;
    cout << "*      4) Finalizar compra                          *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao desejada:" << endl << "--> ";
    cin >> menu_carrinho;
    cout << endl;

    return menu_carrinho;
}

/**
* @brief Função que imprime o menu do cliente com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_cliente () {
    int menu_cliente;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                     Menu Cliente                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Consultar Produtos                        *" << endl;
    cout << "*      2) Carrinho de Compras                       *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao desejada:" << endl << "--> ";
    cin >> menu_cliente;
    cout << endl;

    return menu_cliente;
}