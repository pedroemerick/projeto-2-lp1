/**
* @file	    nota_fiscal.cpp
* @brief	Arquivo com a implementação dos metodos da classe Nota_fiscal
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
* @sa       nota_fiscal.h
*/

#include <iostream>
using std:: cout;
using std:: endl;

#include "nota_fiscal.h"
#include "item_produto.h"
#include "ll_dupla_ord.h"
#include "funcoes_lista.h"
#include <cstdlib>
#include <ctime>

/**
 * @details O valor total é iniciado com zero, e o valor de numero é inicializado 
 *          aleatoriamente com um valor ate 1000
 */
Nota_fiscal::Nota_fiscal () {

    srand(time(NULL));

    numero = rand() % 1000;
    total = 0.0;
}

/**
 * @return Total da compra
 */
float Nota_fiscal::getTotal () {
    return total;
}

/**
 * @return Numero da nota fiscal
 */
int Nota_fiscal::getNumero () {
    return numero;
}

/**
 * @return Lista com produtos do carrinho
 */
Lista <Item_produto>* Nota_fiscal::RetornaItens () {
    return &itens_produtos;
}

/**
 * @details O metodo adiciona um item ao carrinho
 * @param   p Item a ser adicionado
 */
void Nota_fiscal::AddItem (Item_produto &p) {
    itens_produtos.Inserir(p);
}

/**
 * @details O metodo remove um item ao carrinho
 * @param   nome Nome do item a ser removido
 */
void Nota_fiscal::RemoverItem (string nome) {
    Item_produto *aux = retorna_item (itens_produtos, nome);
    itens_produtos.Remover(*aux);
}

/**
 * @details O metodo limpa o carrinho de compras 
 *          e retorna os valores de total e numero como padrao para o proximo cliente usar
 */
void Nota_fiscal::FinalizarCompra () {
    limpar_carrinho (itens_produtos);
    total = 0;
    srand(time(NULL));
    numero = rand() % 1000;
}

/**
 * @details Não ocorre nada pois nao tem variaveis para serem destruidas
 */
Nota_fiscal::~Nota_fiscal  () {

}