/**
 * @file	produto_perecivel.cpp
 * @brief	Implementacao dos metodos da classe Perecivel
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	05/06/2017
 * @sa		produto_perecivel.h
 */

#include "produto_perecivel.h"
#include "data.h"

#include <iostream>
using std:: cout;
using std:: endl;

#include <ctime>

/**
 * @details O valor da data já é iniciado por padrao na classe Data
 */
Perecivel::Perecivel () {

}

/**
 * @details O valor do dado do produto perecivel é recebido por parametro
 * @param   v Validade para o produto
 */
Perecivel::Perecivel (Data v) {
    validade = v;
}

/**
 * @return Se um produto está dentro da data de validade ou nao
 */
bool Perecivel::ConsumoProduto () {

        struct tm *horario_local;
        time_t t;
            
        t = time(NULL);                     // Obtem informações de data e hora
        horario_local = localtime(&t);      // Converte a hora atual para a hora local

        int dia = horario_local->tm_mday;
        int mes = horario_local->tm_mon + 1;
        int ano = horario_local->tm_year + 1900;

        cout << endl << "Data de validade: " << validade.getDia () << "/" << validade.getMes() << "/" << validade.getAno () << endl;
        cout << "Data Atual: " << dia << "/" << mes << "/" << ano << endl << endl;

        if (ano > validade.getAno ()) {
            return false;
        }
        else if (ano == validade.getAno ()){
            if (mes > validade.getMes()) {
                return false;
            }
            else if (mes == validade.getMes()) {
                if (dia > validade.getDia ()) {
                    return false;
                }
            }
        }

    return true;
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Perecivel::~Perecivel () {

}