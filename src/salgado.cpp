/**
 * @file	salgado.cpp
 * @brief	Implementacao dos metodos da classe Salgado
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		salgado.h
 */

#include "data.h"
#include "salgado.h"
#include "produto.h"
#include "produto_perecivel.h"
#include "menus.h"

#include <string>
using std:: string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details O valor de tipo é iniciado com "salgado", o valor de quantidade de sodio
 *          iniciado com zero e a composicao inicado com um espaco em branco
 */
Salgado::Salgado () {
    qtd_sodio = 0;
    composicao = " ";
    tipo = "salgado";
}


/**
 * @details Os valores dos dados do salgado sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   s Uuantidade de sodio do salgado
 * @param   cp Composicao do salgado
 * @param   f Fornecedor
 * @param   v Data de validade da bebida
 */
Salgado::Salgado (string n, double c, string d, float p, int qe, float s, string cp, Fornecedor &f, Data v) {
    tipo = "salgado";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;
    
    qtd_sodio = s;
    composicao = cp;

    validade = v;
}

/**
 * @return Quantidade de sodio do livro
 */
float Salgado::getQtd_sodio () {
    return qtd_sodio;
}

/**
 * @details O metodo modifica a quantidade de sodio do salgado
 * @param   qs Quantidade de sodio para o salgado
 */
void Salgado::setQtd_sodio (float qs) {
    qtd_sodio = qs;
}

/**
 * @return Composicao do salgado
 */
string Salgado::getComposicao () {
    return composicao;
}

/**
 * @details O metodo modifica a composicao do salgado
 * @param   cp Composicao do salgado
 */
void Salgado::setComposicao (string cp) {
    composicao = cp;
}

/** 
 * @details O metodo coloca os dados do salgado em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados do salgado
 */
string Salgado::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << qtd_sodio << ";" << composicao << ";" << validade.To_string () << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado do salgado escolhido pelo usuario
 */
void Salgado::AlteraSalgado () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_salgado ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite a nova quantidade de sodio do produto (em miligramas): ";
            cin >> qtd_sodio;
        }
        else if (opcao == 7)
        {
            cout << "Digite a nova composicao do produto (lactose e/ou gluten): ";
            getline (cin >> ws, composicao);
        }
        else if (opcao < 0 || opcao > 7)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Salgado::~Salgado () {
    
}