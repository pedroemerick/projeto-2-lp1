/**
 * @file	bebida.cpp
 * @brief	Implementacao dos metodos da classe Bebida
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		bebida.h
 */

#include "bebida.h"
#include "produto.h"
#include "data.h"
#include "produto_perecivel.h"
#include "menus.h"

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

#include <sstream>
using std::ostringstream;

/**
 * @details O valor de tipo é iniciado com "bebida", os valores de teor alcoolico e 
 *          quantidade de acucar sao inicializados com zero
 */
Bebida::Bebida () {
    teor_alcoolico = 0;
    qtd_acucar = 0;
    tipo = "bebida";
}

/**
 * @details Os valores dos dados da bebida sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   t Teor alcoolico da bebida
 * @param   q Quantidade de acucar da bebida
 * @param   f Fornecedor
 * @param   v Data de validade da bebida
 */
Bebida::Bebida (string n, double c, string d, float p, int qe, float t, float q, Fornecedor &f, Data v) {
    tipo = "bebida";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;

    teor_alcoolico = t;
    qtd_acucar = q;

    validade = v;
}

/**
 * @return Teor alcoolico da bebida
 */
float Bebida::getTeor_alcoolico () {
    return teor_alcoolico;
}

/**
 * @details O metodo modifica o teor alcoolico da bebida
 * @param   ta Teor alcoolico para a bebida
 */
void Bebida::setTeor_alcoolico (float ta) {
    teor_alcoolico = ta;
}

/**
 * @return Quantidade de acucar da bebida
 */
float Bebida::getQtd_acucar () {
    return qtd_acucar;
}

/**
 * @details O metodo modifica a quantidade de acucar da bebida
 * @param   qa Quantidade de acucar para a bebida
 */
void Bebida::setQtd_acucar (float qa) {
    qtd_acucar = qa;
}

/** 
 * @details O metodo coloca os dados da bebida em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados da bebida
 */
string Bebida::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << teor_alcoolico << ";" << qtd_acucar << ";" << validade.To_string () << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado da bebida escolhido pelo usuario
 */
void Bebida::AlteraBebida () {
    
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_bebida ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite a nova porcentagem de teor alcoolico do produto: ";
            cin >> teor_alcoolico;
        }
        else if (opcao == 7)
        {
            cout << "Digite a nova quantidade de acucar do produto (em miligramas): ";
            cin >> qtd_acucar;
        }
        else if (opcao < 0 || opcao > 7)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Bebida::~Bebida () {

}