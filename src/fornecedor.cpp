/**
 * @file	fornecedor.cpp
 * @brief	Implementacao dos metodos da classe Fornecedor
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	05/06/2017
 * @sa		fornecedor.h
 */

#include "fornecedor.h"

#include <string>
using std:: string;

#include <ostream>
using std::ostream; 

#include <iostream>
using std::endl;

#include <sstream>
using std::ostringstream;

/**
 * @details O valor de nome é inicializado com um espaço e cnpj com zero
 */
Fornecedor::Fornecedor () {
    nome = " ";
    cnpj = 0;
}

/**
 * @details Os valores dos dados do fornecedor sao recebidos por parametro
 * @param   n Nome para o fornecedor
 * @param   c CNPJ para o fornecedor
 */
Fornecedor::Fornecedor (string n, long int c) {
    nome = n;
    cnpj = c;
}

/**
 * @details O metodo modifica o nome do fornecedor
 * @param   n Nome para o fornecedor
 */
void Fornecedor::setNome (string n) {
    nome = n;
}

/**
 * @return CNPJ do fornecedor
 */
string Fornecedor::getNome () {
    return nome;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao se é menor
 *			um instante de Fornecedor a outro
 * @param	f Instante de Fornecedor que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é menor que o outro
 */
bool Fornecedor::operator < (Fornecedor const f) {
    if (nome < f.nome)
        return true;
    
    return false;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de Fornecedor a outro
 * @param	f Instante de Fornecedor que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é igual ao outro
 */
bool Fornecedor::operator == (Fornecedor const f) {
    if (nome == f.nome) {
        return true;
    }

    return false;
}

/** 
 * @details O operador e sobrecarregado para representar um Fornecedor
 *			com os dados do fornecedor
 * @param	o Referencia para stream de saida
 * @param	f Referencia para o objeto Fornecedor a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &o, Fornecedor const &f ) {

    o << endl << "Nome do fornecedor: " << f.nome << endl;
    o << "CNPJ: " << f.cnpj << endl;

    return o;    
}

/** 
 * @details O metodo coloca os dados do fornecedor em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados do fornecedor
 */
string Fornecedor::To_string () {
    ostringstream oss;

    oss << nome << ";" << cnpj << endl;

    return oss.str ();
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Fornecedor::~Fornecedor () {

}