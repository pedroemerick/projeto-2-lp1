/**
* @file	    funcoes_cliente.cpp
* @brief	Arquivo com as funções referente as manipulações dos produtos feitas pelo cliente.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
*/

#include "ll_dupla_ord.h"
#include "funcoes_cliente.h"
#include "nota_fiscal.h"
#include "menus.h"
#include "consulta.h"
#include "funcoes_lista.h"
#include "bebida.h"
#include "fruta.h"
#include "salgado.h"
#include "doce.h"
#include "arquivos.h"

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <fstream>
using std::ofstream;

#include <sstream>
using std::stringstream;

#include <sstream>
using std::ws;

/**
* @brief Função que imprime o menu do carrinho de comprar do clente.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void carrinho_compras (Nota_fiscal &carrinho, Lista <Produto*> &produtos ) {
    int opcao = -1;

    do {

        opcao = menu_carrinho ();
        system("clear");

        switch (opcao) {

            case 1:
                mostrar_carrinho(carrinho);              
                break;
            case 2:
                inserir_produtos (carrinho, produtos);
                break;
            case 3:
                remover_produtos (carrinho, produtos);
                break;
            case 4:
                finalizar_compra (carrinho);
                opcao = 0;
                break;
            case 0:
                system("clear");
                break;
            default:
                cout << "Opção inválida!" << endl;
                break;        
        }

    } while (opcao != 0);

}

/**
* @brief Função que insere no carrinho o item escolhido pelo usuário.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void inserir_produtos (Nota_fiscal &carrinho, Lista <Produto*> &produtos ) {

    string nome_produto;    
    int quant;

    cout << "Digite o nome do produto: " << endl << "--> ";
    getline (cin >> ws, nome_produto);

    Produto *p = retorna_produto (produtos, nome_produto);

    if (p == NULL) {
        cout << endl << "Não há o produto '" << nome_produto << "' no estoque!" << endl;
        return;
    }

    string tipo = p->getTipo ();

    if (tipo == "bebida") {

        Bebida *temp = dynamic_cast <Bebida*> (p);
        
        if (!temp->ConsumoProduto ()) {
            cout << "Notamos que o produto '" << p->getNome() << "' está fora do prazo de validade. Portanto, não será";
            cout << " possível realizar a venda do mesmo. Solicitamos que aguarde a ";
            cout << " reposição de um novo lote. Agradecemos a compreensão!" << endl << endl << "Equipe QLeveTudo" << endl;
            return;
        }
    }
    else if (tipo == "fruta") {

        Fruta *temp = dynamic_cast <Fruta*> (p);
        
        if (!temp->ConsumoProduto ()) {
            cout << "Notamos que o produto '" << p->getNome() << "' está fora do prazo de validade. Portanto, não será";
            cout << " possível realizar a venda do mesmo. Solicitamos que aguarde a";
            cout << " reposição de um novo lote. Agradecemos a compreensão!" << endl << endl << "Equipe QLeveTudo" << endl;
            return;
        }
    }
    else if (tipo == "Salgado") {

        Salgado *temp = dynamic_cast <Salgado*> (p);
        
        if (!temp->ConsumoProduto ()) {
            cout << "Notamos que o produto '" << p->getNome() << "' está fora do prazo de validade. Portanto, não será";
            cout << " possível realizar a venda do mesmo. Solicitamos que aguarde a";
            cout << " reposição de um novo lote. Agradecemos a compreensão!" << endl << endl << "Equipe QLeveTudo" << endl;
            return;
        }
    }
    else if (tipo == "Doce") {

        Doce *temp = dynamic_cast <Doce*> (p);
        
        if (!temp->ConsumoProduto ()) {
            cout << "Notamos que o produto '" << p->getNome() << "' está fora do prazo de validade. Portanto, não será";
            cout << " possível realizar a venda do mesmo. Solicitamos que aguarde a";
            cout << " reposição de um novo lote. Agradecemos a compreensão!" << endl << endl << "Equipe QLeveTudo" << endl;
            return;
        }
    }

    Item_produto *i = retorna_item (*(carrinho.RetornaItens ()), nome_produto);

    if (i == NULL) {
        cout << endl << "Digite a quantidade do produto '" << nome_produto << "' que deseja" << endl << "--> ";
        cin >> quant;

        if (p->getQtd_estoque () < quant) {
            cout << endl << "Infelizmente nao temos esta quantidade de produtos em estoque !!! Temos apenas ";
            cout << p->getQtd_estoque () << " unidades em estoque do produto !!!" << endl;

            return;
        }

        Item_produto aux(*p, quant);

        carrinho.AddItem (aux);

        p->setQtd_estoque (p->getQtd_estoque () - quant);

        cout << endl << "Produto(os) '" << nome_produto << "' adicionado(os) ao carrinho!" << endl;
    }
    else {
        cout << endl << "Digite a quantidade do produto '" << nome_produto << "' que deseja" << endl << "--> ";
        cin >> quant;

        if ((p->getQtd_estoque ()) < (i->getQtd_produtos() + quant)) {
            cout << endl << "Infelizmente nao temos esta quantidade de produtos em estoque !!! Temos apenas ";
            cout << p->getQtd_estoque () << " unidades em estoque do produto !!!" << endl;

            return;
        }

        p->setQtd_estoque (p->getQtd_estoque () + quant);

        quant += i->getQtd_produtos();

        i->setQtd_produtos(quant);

        p->setQtd_estoque (p->getQtd_estoque () - quant);

        cout << endl << "Produto(os) '" << nome_produto << "' adicionado(os) ao carrinho!" << endl;
    }
}

/**
* @brief Função que remove um item do carrinho de compras.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void remover_produtos (Nota_fiscal &carrinho, Lista <Produto*> &produtos) {

    if (!consultar_qtd_itens_carrinho(carrinho)) {
        cout << "Não há produtos em seu carrinho de compras!" << endl;
        return;
    }

    string nome_produto;
    int quant;

    cout << "Digite o nome do produto: " << endl << "--> ";
    getline (cin >> ws, nome_produto);

    Item_produto *i = retorna_item (*(carrinho.RetornaItens ()), nome_produto);

    if (i == NULL) {
        cout << endl << "Não há o produto '" << nome_produto << "' no carrinho de compras!" << endl;
        return;
    }

    cout << endl << "Digite a quantidade do produto '" << nome_produto << "' que deseja remover" << endl << "--> ";
    cin >> quant;

    if (i->getQtd_produtos() > quant) {
        i->setQtd_produtos( (i->getQtd_produtos() - quant) );

        Produto *p = retorna_produto (produtos, nome_produto);
        p->setQtd_estoque (p->getQtd_estoque () + quant);
        
        cout << endl << "Produtos removidos do carrinho de compras!" << endl;        
        return;
    }

    else if (i->getQtd_produtos() < quant) {
        cout << endl << "Não há toda essa quantidade de '" << nome_produto << "' no carrinho!" << endl;
        return;
    }

    carrinho.RemoverItem (nome_produto);
    if (quant > 1) {
        cout << endl << "Produtos removidos do carrinho de compras!" << endl;
    }
    else {
        cout << endl << "Produto removido do carrinho de compras!" << endl;
    }
    
}

/**
* @brief Função que gera a nota fiscal com todos os dados da compra.
* @param carrinho Objeto que contem os dados da compra.
*/
void finalizar_compra (Nota_fiscal &carrinho) {

    if (!consultar_qtd_itens_carrinho(carrinho)) {
        cout << endl <<  "Não há produtos em seu carrinho de compras!" << endl << endl;
        return;
    }

    struct tm *horario_local;
    time_t t;
        
    t = time(NULL);                     // Obtem informações de data e hora
    horario_local = localtime(&t);      // Converte a hora atual para a hora local

    int dia = horario_local->tm_mday;
    int mes = horario_local->tm_mon + 1;
    int ano = horario_local->tm_year + 1900;

    cout << endl;
    cout << "************************************************************************" << endl;
    cout << "*                      Loja de conveniência QLeveTudo                  *" << endl;
    cout << "*                    Lagoa Nova, Natal - RN, 59076-560                 *" << endl;
    cout << "* CNPJ: 93.234.344/0012-04                                             *" << endl;
    cout << "*----------------------------------------------------------------------*" << endl;
    cout << "* " << dia << "/" << mes << "/" << ano << "                                                             *" << endl;
    cout << "*                             NOTA FISCAL                              *" << endl;
    cout << "*                                                                      *" << endl;
    cout << "* Qtd.   Produto               Vl.unit (R$)             Vl.item (R$)   *" << endl;
    cout << "*----------------------------------------------------------------------*" << endl;
    produtos_cupom_fiscal (carrinho);
    cout << endl << " COO: " << carrinho.getNumero() << endl;
    cout << "*----------------------------------------------------------------------*" << endl;
    cout << "* Agradeçemos pela preferência!                                        *" << endl;
    cout << "* Obrigado e volte sempre!                                             *" << endl;
    cout << "************************************************************************" << endl << endl;

    string nota = "./data/";
    stringstream converte;
    converte << carrinho.getNumero ();
    nota += converte.str ();
    nota += ".txt";

    ofstream arquivo (nota);
    saida_nota_fiscal (arquivo, carrinho);
    arquivo.close ();

    carrinho.FinalizarCompra ();
}