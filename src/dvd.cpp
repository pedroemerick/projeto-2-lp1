/**
 * @file	dvd.cpp
 * @brief	Implementacao dos metodos da classe DVD
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		dvd.h
 */

#include "dvd.h"
#include "produto.h"
#include "menus.h"

#include <string>
using std::string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details O valor de tipo é iniciado com "dvd", os valores de titulo e genero 
 *          sao inicializados com escpaco em branco e duracao com zero
 */
DVD::DVD () {
    titulo = " ";
    genero = " ";
    duracao = 0;
    tipo = "dvd";
}

/**
 * @details Os valores dos dados do dvd sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   t Titulo do dvd
 * @param   g Genero do dvd
 * @param   dt Duracao (em minutos) do filme
 * @param   f Fornecedor
 */
DVD::DVD (string n, double c, string d, float p, int qe, string t, string g, int dt, Fornecedor &f) {
    tipo = "dvd";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;

    titulo = t;
    genero = g;
    duracao = dt;
}

/**
 * @return Titulo do dvd
 */
string DVD::getTitulo () {
    return titulo;
}

/**
 * @details O metodo modifica o titulo do dvd
 * @param   t Titulo para o dvd
 */
void DVD::setTitulo (string t) {
    titulo = t;
}

/**
 * @return Genero do dvd
 */
string DVD::getGenero () {
    return genero;
}

/**
 * @details O metodo modifica o genero do dvd
 * @param   g Genero para o dvd
 */
void DVD::setGenero (string g) {
    genero = g;
}

/**
 * @return Duracao do dvd
 */
int DVD::getDuracao () {
    return duracao;
}

/**
 * @details O metodo modifica a duração do dvd
 * @param   d Duração para o dvd
 */
void DVD::setDuracao (int d) {
    duracao = d;
}

/** 
 * @details O metodo coloca os dados do dvd em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados do dvd
 */
string DVD::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << titulo << ";" << genero << ";" << duracao << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado do dvd escolhido pelo usuario
 */
void DVD::AlteraDVD () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_dvd ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite o novo titulo do DVD: ";
            getline (cin >> ws, titulo);
        }
        else if (opcao == 7)
        {
            cout << "Digite o novo genero do DVD: ";
            getline (cin >> ws, genero);
        }
        else if (opcao == 8)
        {
            cout << "Digite a nova duracao do DVD: ";
            cin >> duracao;
        }
        else if (opcao < 0 || opcao > 8)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
DVD::~DVD () {

}