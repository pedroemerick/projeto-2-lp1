/**
* @file     consulta.cpp
* @brief    Implementacao das funcoes que mostram os dados de um produto ou fornecedor de acordo
*           com a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/05/2017
*/

#include "consulta.h"
#include "menus.h"
#include "funcoes_lista.h"
#include "item_produto.h"
#include "nota_fiscal.h"
#include "ll_dupla_ord.h"
#include "fornecedor.h"
#include "produto.h"

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std:: string;

#include <sstream>
using std::ws;

#include <iomanip>

/**
* @brief Função que mostra as opcoes de consulta do cliente e imprime o resultado da consulta desejada
* @param produtos Lista de produtos
*/
void consulta_cliente (Lista <Produto*> &produtos)
{
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_consulta_cliente ();
        system("clear");

        // Chama a funcao para impressao da consulta de acordo com a opcao do usuario
        switch (opcao) {
            case 1: {       // PRODUTO POR NOME
                string nome_produto;
                cout << "Digite o nome do produto que deseja consultar: ";
                getline (cin >> ws, nome_produto);

                mostrar_produto_nome (produtos, nome_produto);
                break;
            }
            case 2:         // PRODUTO POR TIPO
                consulta_tipos (produtos);
                break;
            case 3: {       // TODOS PRODUTOS
                // Percorre a lista de produtos imprimindo todos os produtos
                for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
                {
                    cout << *(*ii->getDadoPtr ());
                }
                break;
            }
            case 0:
                system("clear");
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }   
    } 
}

/**
* @brief Função que mostra as opcoes de consulta por tipo, recebe a opcao do usuario 
*        e mostra os produtos do tipo desejado
* @param produtos Lista de produtos
*/
void consulta_tipos (Lista <Produto*> &produtos) 
{
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_consulta_tipo ();
        system("clear");

        // Chama a funcao de impressao por tipo de acordo com a escolha do usuario
        switch (opcao) {
            case 1:     // Imprimir bebidas
                mostrar_produto_tipo (produtos, "bebida");
                break;
            case 2:     // Imprimir frutas
                mostrar_produto_tipo (produtos, "fruta");
                break;
            case 3:     // Imprimir salgados
                mostrar_produto_tipo (produtos, "salgado");
                break;
            case 4:     // Imprimir doces
                mostrar_produto_tipo (produtos, "doce");
                break;
            case 5:     // Imprimir cds
                mostrar_produto_tipo (produtos, "cd");
                break;
            case 6:     // Imprimir dvds
                mostrar_produto_tipo (produtos, "dvd");
                break;
            case 7:     // Imprimir livros
                mostrar_produto_tipo (produtos, "livro");
                break;
            case 0:
                system("clear");
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }
}

/**
* @brief Função que imprime os produtos de acordo com o tipo desejado
* @param produtos Lista de produtos
* @param tipo Tipo desejado para impressao dos produtos
*/
void mostrar_produto_tipo (Lista <Produto*> &produtos, string tipo)
{
    int cont = 0;

    // Percorre a lista de produtos imprimindo todos os produtos que tem o mesmo 
    // tipo recebido na funcao
    for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        if (((*ii->getDadoPtr ())->getTipo ()) == tipo) {
            cout << *(*ii->getDadoPtr ());
            cont ++;            
        }
    }

    if (cont == 0)
        cout << "Não existem produtos deste tipo !!!" << endl;
}

/**
* @brief Função que imprime os produtos de acordo com o nome do produto
* @param produtos Lista de produtos
* @param nome Nome do produto para impressao
*/
void mostrar_produto_nome (Lista <Produto*> &produtos, string nome)
{
    int cont = 0;

    // Percorre a lista de produtos imprimindo todos os produtos que tem o mesmo 
    // nome recebido na funca
    for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        if (((*ii->getDadoPtr ())->getNome ()) == nome) {
            cout << *(*ii->getDadoPtr ()) << endl;
            cont ++;            
        }
    }

    if (cont == 0)
        cout << endl << "Não existem produtos com este nome !!!" << endl;
}

/**
* @brief Função que mostra as opcoes de consulta do comerciante e imprime o resultado da consulta desejada
* @param produtos Lista de produtos
* @param fornecedores Lista de fornecedores
*/
void consulta_comerciante (Lista <Produto*> &produtos, Lista <Fornecedor> &fornecedores)
{
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_consulta_comerciante ();
        system("clear");

        // Chama a funcao para impressao da consulta de acordo com a opcao do usuario        
        switch (opcao) {
            case 1: {       // PRODUTO POR NOME
                string nome_produto;
                cout << "Digite o nome do produto que deseja consultar: ";
                getline (cin >> ws, nome_produto);

                mostrar_produto_nome (produtos, nome_produto);
                break;
            }
            case 2:         // PRODUTO POR TIPO
                consulta_tipos (produtos);
                break;
            case 3: {       // TODOS PRODUTOS
                for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
                {
                    cout << *(*ii->getDadoPtr ());
                }
                break;
            }
            case 4: {       // PRODUTO POR FORNECEDOR
                string nome_fornecedor;

                cout << "Digite o nome do(a) fornecedor(a): ";
                getline (cin >> ws, nome_fornecedor);

                if (existe_fornecedor (fornecedores, nome_fornecedor) == true)
                {
                    Fornecedor *aux = retorna_fornecedor (fornecedores, nome_fornecedor);

                    mostrar_produto_fornecedor (produtos, *aux);
                }
                else 
                {
                    cout << endl << "Fornecedor(a) nao cadastrado(a) !!!" << endl;
                }
                break;
            }
            case 5: {       // TODOS FORNECEDORES
                for (Node <Fornecedor> *ii = fornecedores.getInicio (); ii != NULL; ii = ii->getProx ())
                {
                    cout << *(ii->getDadoPtr ());
                }
                break;
            }
            case 6: {       // FORNECEDOR POR NOME
                string nome_fornecedor;

                cout << "Digite o nome do(a) fornecedor(a): ";
                getline (cin >> ws, nome_fornecedor);

                mostrar_fornecedor_nome (fornecedores, nome_fornecedor);
                break;
            }
            case 0:
                system("clear");
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }   
    } 
}

/**
* @brief Função que imprime os produtos de acordo com o fornecedor
* @param produtos Lista de produtos
* @param fornecedor Fornecedor para consulta
*/
void mostrar_produto_fornecedor (Lista <Produto*> &produtos, Fornecedor &fornecedor)
{
    int cont = 0;

    // Percorre a lista de produtos imprimindo todos os produtos que tem o mesmo 
    // fornecedor recebido na funcao
    for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        if (((*ii->getDadoPtr ())->getFornecedor ()) == fornecedor) {
            cout << *(*ii->getDadoPtr ());
            cont ++;            
        }
    }

    if (cont == 0)
        cout << endl << "Não existem produtos deste fornecedor !!!" << endl;
}

/**
* @brief Função que imprime o fornecedor de acordo com o nome
* @param fornecedores Lista de fornecedores
* @param nome Nome para impressao do fornecedor
*/
void mostrar_fornecedor_nome (Lista <Fornecedor> &fornecedores, string nome)
{
    int cont = 0;

    // Percorre a lista de fornecedores imprimindo todos os fornecedores que tem o mesmo 
    // nome recebido na funcao
    for (Node <Fornecedor> *ii = fornecedores.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        if (((ii->getDadoPtr ())->getNome ()) == nome) {
            cout << *(ii->getDadoPtr ());
            cont ++;            
        }
    }

    if (cont == 0)
        cout << endl << "Não existe fornecedor com este nome !!!" << endl;
}

/**
* @brief Função que imprime os produtos que estão no carrinho de compras.
* @param &carrinho Referêcia ao objeto carrinho.
*/
void mostrar_carrinho (Nota_fiscal &carrinho) {
    
    Lista <Item_produto>* aux_item = carrinho.RetornaItens ();

    int cont = 0;

    for (Node <Item_produto> *ii = aux_item->getInicio (); ii != NULL; ii = ii->getProx ()) {
            cout << *(ii->getDadoPtr ());
            cout << "Quantidade de produtos: " << (ii->getDadoPtr ())->getQtd_produtos() << endl;
            cont ++;
    }

    if (cont == 0) {

        cout << "Não há produtos em seu carrinho de compras!" << endl;        
    }
}

/**
* @brief Função que imprime o estoque de produtos.
* @param &produtos Referêcia a lista de ponteiros de produtos.
*/
void mostrar_estoque (Lista <Produto*> &produtos) {

    int cont = 0;

    for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ()) {
            cout << *(*ii->getDadoPtr ()) << endl;
            cont ++;            
    }

    if (cont == 0) {
        cout << "Não há produtos no estoque" << endl;        
    }
}

/**
* @brief Função que verifica a quantidade de itens no carriho de compras.
* @param &carrinho Referêcia ao objeto carrinho.
*/
bool consultar_qtd_itens_carrinho (Nota_fiscal &carrinho) {
    
    Lista <Item_produto>* aux_item = carrinho.RetornaItens ();

    int cont = 0;
    Node <Item_produto> *ii = aux_item->getInicio ();

    for (; ii != NULL; ii = ii->getProx ()) {
        cont ++;
    }

    if (cont >= 1) {
        return true;
    }
    
    return false;
}

/**
* @brief Função que imprime as informações necessários para contruir a nota fiscal.
* @param &carrinho Referência ao objeto carrinho.
*/
void produtos_cupom_fiscal (Nota_fiscal &carrinho) {
    
    Lista <Item_produto>* aux_item = carrinho.RetornaItens ();
    int qtd_produtos;
    float preco;

    for (Node <Item_produto> *ii = aux_item->getInicio (); ii != NULL; ii = ii->getProx ()) {
        
        Produto *aux = (ii->getDadoPtr ())->getProduto();
        qtd_produtos = (ii->getDadoPtr ())->getQtd_produtos();
        preco = aux->getPreco () * qtd_produtos;

        cout << "   " << qtd_produtos << "     " << aux->getNome () << "                      ";
        cout << std::setprecision(5) << aux->getPreco () << "                   " << preco << endl;
    }
}