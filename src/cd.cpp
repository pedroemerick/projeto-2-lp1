/**
 * @file	cd.cpp
 * @brief	Implementacao dos metodos da classe CD
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		cd.h
 */

#include "produto.h"
#include "cd.h"
#include "menus.h"

#include <string>
using std::string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details O valor de tipo é iniciado com "cd", os valores de artista, estilo e 
 *          album sao inicializados com espaco em branco
 */
CD::CD () {
    estilo = " ";
    artista = " ";
    album = " ";
    tipo = "cd";
}

/**
 * @details Os valores dos dados do cd sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   e Estilo do cd
 * @param   a Artista do cd
 * @param   ab Album do cd  
 * @param   f Fornecedor
 */
CD::CD (string n, double c, string d, float p, int qe, string e, string a, string ab, Fornecedor &f) {
    tipo = "cd";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;
    
    estilo = e;
    artista = a;
    album = ab;
}

/**
 * @return Estilo do cd
 */
string CD::getEstilo () {
    return estilo;
}

/**
 * @details O metodo modifica o estilo do cd
 * @param   e Estilo para o cd
 */
void CD::setEstilo (string e) {
    estilo = e;
}

/**
 * @return Artista do cd
 */
string CD::getArtista () {
    return artista;
}

/**
 * @details O metodo modifica o artista do cd
 * @param   a Artista para o cd
 */
void CD::setArtista (string a) {
    artista = a;
}

/**
 * @return Album do cd
 */
string CD::getAlbum () {
    return album;
}

/**
 * @details O metodo modifica o album do cd
 * @param   ab Album para o cd
 */
void CD::setAlbum (string ab) {
    album = ab;
}

/** 
 * @details O metodo coloca os dados do cd em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados do cd
 */
string CD::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << estilo << ";" << artista << ";" << album << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado do cd escolhido pelo usuario
 */
void CD::AlteraCD () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_cd ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite o novo estilo do CD: ";
            getline (cin >> ws, estilo);
        }
        else if (opcao == 7)
        {
            cout << "Digite o novo artista do CD: ";
            getline (cin >> ws, artista);
        }
        else if (opcao == 8)
        {
            cout << "Digite o novo album do CD: ";
            getline (cin >> ws, album);
        }
        else if (opcao < 0 || opcao > 8)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}


/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
CD::~CD () {

}
