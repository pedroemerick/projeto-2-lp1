/**
* @file     cadastro_produtos.cpp
* @brief    Implementacao das funcoes que executam o cadastramento de um produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/05/2017
*/

#include "cadastro_produtos.h"
#include "funcoes_lista.h"
#include "data.h"
#include "bebida.h"
#include "cd.h"
#include "doce.h"
#include "dvd.h"
#include "fruta.h"
#include "livro.h"
#include "salgado.h"
#include "fornecedor.h"
#include "produto.h"
#include "ll_dupla_ord.h"

#include <sstream>
using std::ws;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/**
* @brief Função que recebe a opcao do usuario referente ao tipo de produto que deseja cadastrar
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void cadastramento_produto (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_produtos)
{
    int opcao = -1;

    // Mostra menu e recebe a opcao do usuario
    while (opcao != 0)
    {
        opcao = menu_cadastramento ();
        system("clear");

        if (1 <= opcao && opcao <= 7)
            cadastra_produto (fornecedores, produtos, opcao, qtd_produtos);
        else if (opcao == 0)
            return;
        else
            cout << "Opcao invalida !!!" << endl;
    }  
}

/**
* @brief Função que faz a leitura dos dados para o cadastramento de um produto de acordo com o tipo desejado
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param opcao Escolha do tipo de produto para cadastramento
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void cadastra_produto (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int opcao, int &qtd_produtos)
{
    string n_fornecedor;
    string nome;
    double codigo;
    string descricao;
    float preco;
    int qtd_estoque;

    // Recebe o nome do fornecedor do produto e verifica se ele esta cadastrado
    cout << "Digite o nome do(a) fornecedor(a) do produto: ";
    getline (cin >> ws, n_fornecedor);

    if (existe_fornecedor (fornecedores, n_fornecedor) == false)
    {
        cout << endl << "Fornecedor não cadastrado !!!" << endl;
        return;
    }

    Fornecedor *aux = retorna_fornecedor (fornecedores, n_fornecedor);

    cout << endl << "Digite todos os dados pedidos a seguir do produto que deseja cadastrar: " << endl;
    cout << endl << "Nome: ";
    getline (cin >> ws, nome);

    // Verifica se o produto ja foi cadastrado atraves do nome
    if (existe_produto (produtos, nome) == true)
    {
        cout << endl << "Produto já cadastrado !!!" << endl;

        return;
    }

    // Leitura dos dados padroes a todos produtos
    cout << "Codigo: ";
    cin >> codigo;

    cout << "Descricao: ";
    getline (cin >> ws, descricao);

    cout << "Preco: ";
    cin >> preco;

    cout << "Quantidade em estoque: ";
    cin >> qtd_estoque;

    // Leitura dos dados de acordo com o tipo de produto que esta sendo cadastrado
    switch (opcao)
    {
        case 1: {     // BEBIDA
            float teor;
            float qtd_acucar;
            Data validade;

            cout << "Porcentagem de teor alcoolico: ";
            cin >> teor;

            cout << "Quantidade de acucar: ";
            cin >> qtd_acucar;

            cout << "Data de validade (Ex.: 09 05 2017): ";
            cin >> validade;

            produtos.Inserir (new Bebida (nome, codigo, descricao, preco, qtd_estoque, teor, qtd_acucar, *aux, validade));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;
        }
        case 2: {     // FRUTA
            int numero_lote;
            Data producao;
            Data validade;

            cout << "Numero do lote: ";
            cin >> numero_lote;

            cout << "Data da producao do lote (Ex.: 12 05 2016): ";
            cin >> producao;

            cout << "Data de validade (Ex.: 09 05 2017): ";
            cin >> validade;

            produtos.Inserir (new Fruta (nome, codigo, descricao, preco, qtd_estoque, numero_lote, producao, *aux, validade));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;            
        }
        case 3: {     // SALGADO        
            float qtd_sodio;
            string composicao;
            Data validade;

            cout << "Quantidade de sodio: ";
            cin >> qtd_sodio;

            cout << "Composicao (gluten e/ou lactose): ";
            getline (cin >> ws, composicao);

            cout << "Data de validade (Ex.: 09 05 2017): ";
            cin >> validade;

            produtos.Inserir (new Salgado (nome, codigo, descricao, preco, qtd_estoque, qtd_sodio, composicao, *aux, validade));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;
        }
        case 4: {     // DOCE
            float qtd_acucar;
            string composicao;
            Data validade;

            cout << "Quantidade de acucar (em miligramas): ";
            cin >> qtd_acucar;

            cout << "Composicao (gluten e/ou lactose): ";
            getline (cin >> ws, composicao);

            cout << "Data de validade (Ex.: 09 05 2017): ";
            cin >> validade;

            produtos.Inserir (new Doce (nome, codigo, descricao, preco, qtd_estoque, qtd_acucar, composicao, *aux, validade));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;
        }
        case 5: {     // CD
            string estilo;
            string artista;
            string album;

            cout << "Estilo musical: ";
            getline (cin >> ws, estilo);
            
            cout << "Artista: ";
            getline (cin >> ws, artista);
            
            cout << "Album: ";
            getline (cin >> ws, album);

            produtos.Inserir (new CD (nome, codigo, descricao, preco, qtd_estoque, estilo, artista, album, *aux));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;
        }
        case 6: {     // DVD
            string titulo;
            string genero;
            int duracao;

            cout << "Titulo: ";
            getline (cin >> ws, titulo);

            cout << "Genero do DVD: ";
            getline (cin >> ws, genero);

            cout << "Duracao (em minutos): ";
            cin >> duracao;

            produtos.Inserir (new DVD (nome, codigo, descricao, preco, qtd_estoque, titulo, genero, duracao, *aux));
            qtd_produtos += 1;

            cout << endl << "Produto cadastrado com sucesso !!!" << endl;          

            break;
        }
        case 7: {     // LIVRO
            string titulo;
            string autor;
            string editora;
            int ano_publicacao;

            cout << "Titulo: ";
            getline (cin >> ws, titulo);

            cout << "Autor: ";
            getline (cin >> ws, autor);

            cout << "Editora: ";
            getline (cin >> ws, editora);

            cout << "Ano da publicacao: ";
            cin >> ano_publicacao;

            produtos.Inserir (new Livro (nome, codigo, descricao, preco, qtd_estoque, titulo, autor, editora, ano_publicacao, *aux));
            qtd_produtos += 1;
            
            cout << endl << "Produto cadastrado com sucesso !!!" << endl;

            break;
        }
        default:
            break;
    }
}