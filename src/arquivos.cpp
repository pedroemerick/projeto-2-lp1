/**
* @file     arquivos.cpp
* @brief    Implementacao das funcoes que manipulam os arquivos de entrada e saida de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#include "arquivos.h"

#include "produto.h"
#include "bebida.h"
#include "cd.h"
#include "doce.h"
#include "dvd.h"
#include "fruta.h"
#include "livro.h"
#include "salgado.h"
#include "fornecedor.h"
#include "ll_dupla_ord.h"
#include "funcoes_lista.h"
#include "nota_fiscal.h"
#include "consulta.h"

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <iostream>
using std::endl;

#include <cstdlib>
using std::atoi;
using std::atof;
using std::atol;

#include <ctime>

#include <sstream>
using std::ostringstream;

#include <iomanip>

/**
* @brief Função que salva os produtos em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param produtos Lista com todos produtos cadastrados
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void arquivo_saida_produtos (ofstream &arquivo, Lista <Produto*> &produtos, int &qtd_produtos)
{
    string dados_produtos;

    // Escreve a quantidade de produtos no arquivo
    arquivo << qtd_produtos << endl;

    // Percorre a lista de produtos acrescentando todos os dados dos produtos em uma string
    for (Node <Produto*> *ii = produtos.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        dados_produtos += (*(*ii->getDadoPtr ())).To_string ();
    }

    // Escreve todos os produtos com seus dados no arquivo
    arquivo << dados_produtos;
}

/**
* @brief Função que salva os fornecedores em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param fornecedores Lista com todos fornecedores cadastrados
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void arquivo_saida_fornecedores (ofstream &arquivo, Lista <Fornecedor> &fornecedores, int &qtd_fornecedores)
{
    string dados_fornecedores;

    // Escreve a quantidade de fornecedores no arquivo
    arquivo << qtd_fornecedores << endl;

    // Percorre a lista de fornecedpres acrescentando todos os dados dos fornecedores em uma string    
    for (Node <Fornecedor> *ii = fornecedores.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        dados_fornecedores += (*ii->getDadoPtr ()).To_string ();
    }

    // Escreve todos os fornecedores com seus dados no arquivo    
    arquivo << dados_fornecedores;
}

/**
* @brief Função que carrega os fornecedores de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param fornecedores Lista de fornecedores
* @param qtd_fornecedores Quantidade de fornecedores
*/
void carrega_fornecedores (ifstream &arquivo, Lista <Fornecedor> &fornecedores, int &qtd_fornecedores)
{
    string temp;
    int qtd_temp;

    getline (arquivo, temp);
    qtd_temp = atoi (temp.c_str ());
    qtd_fornecedores += atoi (temp.c_str ());

    // Percorre o arquivo pegando os dados de cada fornecedor e inserindo na lista de fornecedores
    for (int ii = 0; ii < qtd_temp; ii++)
    {
        string nome_fornecedor;
        long int cnpj;

        getline (arquivo, nome_fornecedor, ';');
        getline (arquivo, temp);
        cnpj = atol (temp.c_str ());

        fornecedores.Inserir (Fornecedor (nome_fornecedor, cnpj));
    }
}

/**
* @brief Função que carrega os produtos de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos
*/
void carrega_produtos (ifstream &arquivo, Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_produtos)
{
    string temp;
    int qtd_temp;

    getline (arquivo, temp);
    qtd_temp = atoi (temp.c_str ());
    qtd_produtos += atoi (temp.c_str ());

    for (int ii = 0; ii < qtd_temp; ii++) 
    {
        string tipo;
        string nome;
        double codigo;
        string descricao;
        float preco;
        int qtd_estoque;
        string nome_fornecedor;

        // Leitura dos dados padroes em todos produtos
        getline (arquivo, tipo, ';');
        getline (arquivo, nome, ';');
        getline (arquivo, temp, ';');
        codigo = atof (temp.c_str());
        getline (arquivo, descricao, ';');
        getline (arquivo, temp, ';');
        preco = atof (temp.c_str());
        getline (arquivo, temp, ';');
        qtd_estoque = atoi (temp.c_str());
        getline (arquivo, nome_fornecedor, ';');

        // Verificando se o fornecedor do produto ja esta cadastrado no programa
        if (existe_fornecedor (fornecedores, nome_fornecedor) == false)
        {
            cout << endl << "Arquivo com dados, corrompido, existem dados incorretos !!!" << endl;
            return;
        }

        Fornecedor *aux = retorna_fornecedor (fornecedores, nome_fornecedor);

        // Leitura dos dados especificos de cada tipo de produto e inserindo na lista de produtos
        if (tipo == "bebida") {     // Se o produto for do tipo Bebida
            float teor;
            float qtd_acucar;
            Data validade;

            getline (arquivo, temp, ';');
            teor = atof (temp.c_str ());
            getline (arquivo, temp, ';');
            qtd_acucar = atof (temp.c_str ());
            getline (arquivo, temp, '/');
            validade.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            validade.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            validade.setAno (atoi (temp.c_str ()));

            produtos.Inserir (new Bebida (nome, codigo, descricao, preco, qtd_estoque, teor, qtd_acucar, *aux, validade));
        }
        else if (tipo == "fruta") {     // Se o produto for do tipo Fruta
            int numero_lote;
            Data producao_lote;
            Data validade;

            getline (arquivo, temp, ';');
            numero_lote = atoi (temp.c_str ());
            getline (arquivo, temp, '/');
            producao_lote.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            producao_lote.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp, ';');
            producao_lote.setAno (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            validade.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            validade.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            validade.setAno (atoi (temp.c_str ()));

            produtos.Inserir (new Fruta (nome, codigo, descricao, preco, qtd_estoque, numero_lote, producao_lote, *aux, validade));
        }
        else if (tipo == "salgado") {       // Se o produto for do tipo Salgado
            float qtd_sodio;
            string composicao; 
            Data validade;

            getline (arquivo, temp, ';');
            qtd_sodio = atof (temp.c_str ());
            getline (arquivo, composicao, ';');
            getline (arquivo, temp, '/');
            validade.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            validade.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            validade.setAno (atoi (temp.c_str ()));

            produtos.Inserir (new Salgado (nome, codigo, descricao, preco, qtd_estoque, qtd_sodio, composicao, *aux, validade));
        }
        else if (tipo == "doce") {      // Se o produto for do tipo Doce
            float qtd_acucar;
            string composicao; 
            Data validade;

            getline (arquivo, temp, ';');
            qtd_acucar = atof (temp.c_str ());
            getline (arquivo, composicao, ';');
            getline (arquivo, temp, '/');
            validade.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            validade.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            validade.setAno (atoi (temp.c_str ()));

            produtos.Inserir (new Doce (nome, codigo, descricao, preco, qtd_estoque, qtd_acucar, composicao, *aux, validade));
        }
        else if (tipo == "cd") {        // Se o produto for do tipo CD
            string estilo;
            string artista;
            string album;

            getline (arquivo, estilo, ';');
            getline (arquivo, artista, ';');
            getline (arquivo, album);

            produtos.Inserir (new CD (nome, codigo, descricao, preco, qtd_estoque, estilo, artista, album, *aux));
        }
        else if (tipo == "dvd") {       // Se o produto for do tipo DVD
            string titulo;
            string genero;
            int duracao;

            getline (arquivo, titulo, ';');
            getline (arquivo, genero, ';');
            getline (arquivo, temp);
            duracao = atoi (temp.c_str ());

            produtos.Inserir (new DVD (nome, codigo, descricao, preco, qtd_estoque, titulo, genero, duracao, *aux));
        }
        else if (tipo == "livro") {     // Se o produto for do tipo Livro
            string titulo;
            string autor;
            string editora;
            int ano_publicacao;

            getline (arquivo, titulo, ';');
            getline (arquivo, autor, ';');
            getline (arquivo, editora, ';');
            getline (arquivo, temp);
            ano_publicacao = atoi (temp.c_str ());

            produtos.Inserir (new Livro (nome, codigo, descricao, preco, qtd_estoque, titulo, autor, editora, ano_publicacao, *aux));
        }
    }
}

/**
* @brief Função que gera a nota fiscal em um arquivo txt para o cliente
* @param arquivo Arquivo para leitura dos dados
* @param carrinho Carrinho de compras
*/
void saida_nota_fiscal (ofstream &arquivo, Nota_fiscal &carrinho) 
{
    Lista <Item_produto>* aux_item = carrinho.RetornaItens ();
    int qtd_produtos;
    float preco;
    string produtos_nota;

    for (Node <Item_produto> *ii = aux_item->getInicio (); ii != NULL; ii = ii->getProx ()) {
        
        Produto *aux = (ii->getDadoPtr ())->getProduto();
        qtd_produtos = (ii->getDadoPtr ())->getQtd_produtos();
        preco = aux->getPreco () * qtd_produtos;

        ostringstream oss;

        oss << "   " << qtd_produtos << "     " << aux->getNome () << "                      ";
        oss << std::setprecision(5) << aux->getPreco () << "                   " << preco << endl;

        produtos_nota = oss.str ();
    }

    struct tm *horario_local;
    time_t t;
        
    t = time(NULL);                     // Obtem informações de data e hora
    horario_local = localtime(&t);      // Converte a hora atual para a hora local

    int dia = horario_local->tm_mday;
    int mes = horario_local->tm_mon + 1;
    int ano = horario_local->tm_year + 1900;

    arquivo << endl;
    arquivo << "************************************************************************" << endl;
    arquivo << "*                      Loja de conveniência QLeveTudo                  *" << endl;
    arquivo << "*                    Lagoa Nova, Natal - RN, 59076-560                 *" << endl;
    arquivo << "* CNPJ: 93.234.344/0012-04                                             *" << endl;
    arquivo << "*----------------------------------------------------------------------*" << endl;
    arquivo << "* " << dia << "/" << mes << "/" << ano << "                                                             *" << endl;
    arquivo << "*                             NOTA FISCAL                              *" << endl;
    arquivo << "*                                                                      *" << endl;
    arquivo << "* Qtd.   Produto               Vl.unit (R$)             Vl.item (R$)   *" << endl;
    arquivo << "*----------------------------------------------------------------------*" << endl;
    arquivo << produtos_nota;
    arquivo << endl << " COO: " << carrinho.getNumero() << endl;
    arquivo << "*----------------------------------------------------------------------*" << endl;
    arquivo << "* Agradeçemos pela preferência!                                        *" << endl;
    arquivo << "* Obrigado e volte sempre!                                             *" << endl;
    arquivo << "************************************************************************" << endl << endl;
}