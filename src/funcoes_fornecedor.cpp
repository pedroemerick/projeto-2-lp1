/**
* @file     funcoes_fornecedor.cpp
* @brief    Implementacao das funcoes que cadastram e removem um fornecedor
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/05/2017
*/

#include "funcoes_fornecedor.h"
#include "funcoes_lista.h"
#include "fornecedor.h"
#include "produto.h"
#include "ll_dupla_ord.h"

#include <sstream>
using std::ws;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/**
* @brief Função que recebe os dados necessarios de um fornecedor e cadastra no programa 
*        se nao houver outro fornecedor com o mesmo nome
* @param fornecedores Lista de fornecedores
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void cadastramento_fornecedor (Lista <Fornecedor> &fornecedores, int &qtd_fornecedores)
{
    string nome_fornecedor;
    long int cnpj;

    // Recebe os dados do fornecedor
    cout << "Digite o nome do(a) fornecedor(a) que deseja cadastrar: ";
    getline (cin >> ws, nome_fornecedor);

    cout << "Digite o cnpj do(a) fornecedor(a) que deseja cadastrar: ";
    cin >> cnpj;

    // Verifica se o fornecedor ja existe, se nao, inseri na lista de fornecedores
    if (existe_fornecedor (fornecedores, nome_fornecedor) == false)
    {
        fornecedores.Inserir (Fornecedor (nome_fornecedor, cnpj));
        qtd_fornecedores += 1;

        cout << endl << "Fornecedor(a) cadastrado(a) com sucesso !!!" << endl;
    }
    else
    {
        cout << endl << "Fornecedor(a) já é cadastrado(a) !!!" << endl;
    }
} 

/**
* @brief Função que recebe o nome de um fornecedor e o remove do programa se nao houver
*        nenhum produto já cadastrado com ele fornecendo
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void remocao_fornecedor (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_fornecedores)
{
    string nome_fornecedor;

    // Recebe o nome do fornecedor para remocao
    cout << "Digite o nome do(a) fornecedor(a) que deseja remover: ";
    getline (cin >> ws, nome_fornecedor);

    // Verifica se o fornecedor existe
    if (existe_fornecedor (fornecedores, nome_fornecedor) == true)
    {
        Fornecedor *aux = retorna_fornecedor (fornecedores, nome_fornecedor);

        // Verifica se existe algum produto fornecido por ele, se nao, remove da lista de fornecedores
        if (verifica_fornecedor (produtos, *aux) == false)
        {
            remove_fornecedor_nome (fornecedores, nome_fornecedor);
            qtd_fornecedores -= 1;

            cout << endl << "Fornecedor(a) removido(a) com sucesso !!!" << endl;
        }
        else 
        {
            cout << endl << "Existem produtos cadastrados deste(a) fornecedor(a), não é possivel exclui-lo !!!" << endl;
        }
    }
    else 
    {
        cout << endl << "Fornecedor(a) não cadastrado(a) !!!" << endl;
    }
}