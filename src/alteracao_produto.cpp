/**
* @file     alteracao_produto.cpp
* @brief    Implementacao da funcao que chama os metodos de alteracao de dados 
*           de acordo com o tipo do produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    05/06/2017
*/

#include "alteracao_produto.h"

#include "ll_dupla_ord.h"
#include "produto.h"
#include "bebida.h"
#include "cd.h"
#include "doce.h"
#include "dvd.h"
#include "fruta.h"
#include "livro.h"
#include "salgado.h"
#include "funcoes_lista.h"

#include <string>
using std:: string;

#include <sstream>
using std::ws;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/**
* @brief Função que recebe qual produto o usuario deseja alterar os dados 
*        e chama o respectivo metodo para a alteracao de dados
* @param produtos Lista com todos produtos cadastrados
*/
void alteracao_produto (Lista <Produto*> &produtos)
{
    string nome_produto;

    // Recebe o nome do produto para alteracao
    cout << "Digite o nome do produto que deseja fazer as alterações: ";
    getline (cin >> ws, nome_produto);

    // Verifica se o produto existe
    if (existe_produto (produtos, nome_produto) == false)
    {
        cout << endl << "Produto nao cadastrado !!!" << endl;
        return;
    }

    // Retorna o produto com todos os dados
    Produto* aux = retorna_produto (produtos, nome_produto);

    // Faz o cast de acordo com o tipo e chama seu metodo de alteracao
    if (aux->getTipo () == "bebida") {              // Se for uma bebida
        cout << *aux;
        Bebida* temp = dynamic_cast <Bebida*> (aux);
        temp->AlteraBebida ();
    }
    else if (aux->getTipo () == "fruta") {          // Se for uma fruta
        cout << *aux;
        Fruta* temp = dynamic_cast <Fruta*> (aux);
        temp->AlteraFruta ();
    }
    else if (aux->getTipo () == "salgado") {        // Se for um salgado
        cout << *aux;
        Salgado* temp = dynamic_cast <Salgado*> (aux);
        temp->AlteraSalgado ();
    }
    else if (aux->getTipo () == "doce") {           // Se for um doce
        cout << *aux;
        Doce* temp = dynamic_cast <Doce*> (aux);
        temp->AlteraDoce ();
    }
    else if (aux->getTipo () == "cd") {             // Se for um cd
        cout << *aux;
        CD* temp = dynamic_cast <CD*> (aux);
        temp->AlteraCD ();
    }
    else if (aux->getTipo () == "dvd") {            // Se for um dvd
        cout << *aux;
        DVD* temp = dynamic_cast <DVD*> (aux);
        temp->AlteraDVD ();
    }
    else if (aux->getTipo () == "livro") {          // Se for um livro
        cout << *aux;
        Livro* temp = dynamic_cast <Livro*> (aux);
        temp->AlteraLivro ();
    }
}