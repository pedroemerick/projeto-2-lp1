/**
 * @file	doce.cpp
 * @brief	Implementacao dos metodos da classe Doce
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		doce.h
 */

#include "data.h"
#include "doce.h"
#include "produto.h"
#include "produto_perecivel.h"
#include "menus.h"

#include <string>
using std:: string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details O valor de tipo é iniciado com "doce", os valor de quantidade de acucar 
 *          é inicializado com zero e o valor de composicao é inicializado com um espaço em branco       
 */
Doce::Doce () {
    qtd_acucar = 0;
    composicao = " ";
    tipo = "doce";
}

/**
 * @details Os valores dos dados do doce sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   q Quantidade de acucar do doce
 * @param   cp Composicao do doce
 * @param   f Fornecedor
 * @param   v Data de validade do doce
 */
Doce::Doce (string n, double c, string d, float p, int qe, float q, string cp, Fornecedor &f, Data v) {
    tipo = "doce";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;
    
    qtd_acucar = q;
    composicao = cp;

    validade = v;
}

/**
 * @return Quantidade de acucar do doce
 */
float Doce::getQtd_acucar () {
    return qtd_acucar;
}

/**
 * @details O metodo modifica a quantidade de acucar do doce
 * @param   qa Quantidade de acucar para o doce
 */
void Doce::setQtd_acucar (float qa) {
    qtd_acucar = qa;
}

/**
 * @return Composicao do doce
 */
string Doce::getComposicao () {
    return composicao;
}

/**
 * @details O metodo modifica a composicao do doce
 * @param   cp Composicao para o doce
 */
void Doce::setComposicao (string cp) {
    composicao = cp;
}

/** 
 * @details O metodo coloca os dados do doce em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados do doce
 */
string Doce::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << qtd_acucar << ";" << composicao << ";" << validade.To_string () << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado do doce escolhido pelo usuario
 */
void Doce::AlteraDoce () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_doce ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite a nova quantidade de acucar do produto (em miligramas): ";
            cin >> qtd_acucar;
        }
        else if (opcao == 7)
        {
            cout << "Digite a nova composicao do produto (lactose e/ou gluten): ";
            getline (cin >> ws, composicao);
        }
        else if (opcao < 0 || opcao > 7)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Doce::~Doce () {
    
}