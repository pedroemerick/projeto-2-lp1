/**
* @file	    item_produto.cpp
* @brief	Arquivo com as implementações dos metodos da classe item_produto
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
* @sa		item_produto.h
*/

#include <ostream>
using std::ostream;

#include "item_produto.h"
#include "produto.h"

/**
 * @details O valor de quantidade de itens é inicializado com zero
 */
Item_produto::Item_produto () {
    qtd_itens = 0;
}

/**
 * @details Os valores dos dados do Item_produto sao recebidos por parametro
 * @param   p Produto
 * @param   qtd Quantidade de itens
 */
Item_produto::Item_produto (Produto &p, int qtd) {
    item_p_produto = &p;
    qtd_itens = qtd;
}

/**
 * @return Um produto
 */
Produto* Item_produto::getProduto () {
    return item_p_produto;
}

/**
 * @return Quantidade de produtos
 */
int Item_produto::getQtd_produtos () {
    return qtd_itens;
}

/**
 * @details O metodo modifica quantidade de itens
 * @param   q Quantidade de itens desejada
 */
void Item_produto::setQtd_produtos(int q) {
    qtd_itens = q;
}

/** 
 * @details O operador e sobrecarregado para representar um item
 *			com os dados do Item_produto
 * @param	os Referencia para stream de saida
 * @param	i Referencia para o objeto Item_produto a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream& os, const Item_produto &i) {
    os << (*(i.item_p_produto));

    return os;

}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao se é menor
 *			um instante de Item_produto a outro
 * @param	i Instante de Item_produto que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é menor que o outro
 */
bool Item_produto::operator<(const Item_produto &i) {
    if ((*item_p_produto) < (*(i.item_p_produto))) {
        return true;
    }

    return false;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de Item_produto a outro
 * @param	i Instante de Item_produto que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é igual ao outro
 */
bool Item_produto::operator==(const Item_produto &i)
{
    if ((*item_p_produto) == (*(i.item_p_produto))) {
        return true;
    }

    return false;
}

/**
 * @details Não ocorre nada pois nao tem variaveis para serem destruidas
 */
Item_produto::~Item_produto () {

}