/**
 * @file	livro.cpp
 * @brief	Implementacao dos metodos da classe Livro
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		livro.h
 */

#include "produto.h"
#include "livro.h"
#include "menus.h"

#include <string>
using std::string;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details O valor de tipo é iniciado com "livro", os valores de titulo, autor e editora
 *          são inicializados com espaco em branco e ano de publicacao com zero
 */
Livro::Livro () {
    titulo = " ";
    autor = " ";
    editora = " ";
    ano_publicacao = 0;
    tipo = "livro";
}

/**
 * @details Os valores dos dados da bebida sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   t Titulo do livro
 * @param   a Autor do livro
 * @param   e Editora do livro
 * @param   ap Ano de publicacao do livro
 * @param   f Fornecedor
 */
Livro::Livro (string n, double c, string d, float p, int qe, string t, string a, string e, int ap, Fornecedor &f) {
    tipo = "livro";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;

    titulo = t;
    autor = a;
    editora = e;
    ano_publicacao = ap;
}

/**
 * @return Titulo do livro
 */
string Livro::getTitulo () {
    return titulo;
}

/**
 * @details O metodo modifica o titulo do livro
 * @param   t Titulo para o livro
 */
void Livro::setTitulo (string t) {
    titulo = t;
}

/**
 * @return Autor do livro
 */
string Livro::getAutor () {
    return autor;
}

/**
 * @details O metodo modifica o autor do livro
 * @param   a Autor para o livro
 */
void Livro::setAutor (string a) {
    autor = a;
}

/**
 * @return Editora do livro
 */
string Livro::getEditora () {
    return editora;
}

/**
 * @details O metodo modifica a editora do livro
 * @param   e Editora para o livro
 */
void Livro::setEditora (string e) {
    editora = e;
}

/**
 * @return ano de publicacao do livro
 */
int Livro::getAno_publicacao () {
    return ano_publicacao;
}

/**
 * @details O metodo modifica o ano de publicacao do livro
 * @param   ap Ano de publicacao para o livro
 */
void Livro::setAno_publicacao (int ap) {
    ano_publicacao = ap;
}

/** 
 * @details O metodo coloca os dados do livro em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados da livro
 */
string Livro::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << titulo << ";" << autor << ";" << editora << ";" << ano_publicacao << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado do livro escolhido pelo usuario
 */
void Livro::AlteraLivro () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_livro ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite o novo titulo do livro: ";
            getline (cin >> ws, titulo);
        }
        else if (opcao == 7)
        {
            cout << "Digite o novo autor do livro: ";
            getline (cin >> ws, autor);
        }
        else if (opcao == 8)
        {
            cout << "Digite a nova editora do livro: ";
            getline (cin >> ws, editora);
        }
        else if (opcao == 9)
        {
            cout << "Digite o novo ano de publicação do livro: ";
            cin >> ano_publicacao;
        }
        else if (opcao < 0 || opcao > 9)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Livro::~Livro () {
    
}