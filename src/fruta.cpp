/**
 * @file	fruta.cpp
 * @brief	Implementacao dos metodos da classe Fruta
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	04/06/2017
 * @sa		fruta.h
 */

#include "fruta.h"
#include "data.h"
#include "produto.h"
#include "produto_perecivel.h"
#include "menus.h"

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ostringstream;

/**
 * @details O valor de tipo é iniciado com "fruta" e o valor de numero do lote com zero
 */
Fruta::Fruta () {
    numero_lote = 0;
    tipo = "fruta";
}

/**
 * @details Os valores dos dados da bebida sao recebidos por parametro
 * @param   n Nome
 * @param   c Codigo
 * @param   d Descricao
 * @param   p Preco
 * @param   qe Quantidade em estoque
 * @param   nl Numero do lote da fruta
 * @param   pl Data de producao do lote
 * @param   f Fornecedor
 * @param   v Data de validade da bebida
 */
Fruta::Fruta (string n, double c, string d, float p, int qe, int nl, Data pl, Fornecedor &f, Data v) {
    tipo = "fruta";
    nome = n;
    codigo = c;
    descricao = d;
    preco = p;
    qtd_estoque = qe;
    fornecedor = f;
    
    numero_lote = nl;
    producao_lote = pl;

    validade = v;
}

/**
 * @return Numero do lote da fruta
 */
int Fruta::getNumero_lote () {
    return numero_lote;
}

/**
 * @details O metodo modifica o numero do lote da fruta
 * @param   nl Numero do lote para a fruta
 */
void Fruta::setNumero_lote (int nl) {
    numero_lote = nl;
}

/**
 * @return Data de producao do lote da fruta
 */
Data Fruta::getProducao_lote () {
    return producao_lote;
}

/**
 * @details O metodo modifica a data de producao do lote da fruta
 * @param   pl Data de producao do lote para a fruta
 */
void Fruta::setProducao_lote (Data pl) {
    producao_lote = pl;
}

/** 
 * @details O metodo coloca os dados da fruta em uma string,
 *          com os dados separados por ';'
 * @return	String com os dados da fruta
 */
string Fruta::To_string () {
    ostringstream oss;

    oss << tipo << ";" << nome << ";" << codigo << ";" << descricao << ";" << preco << ";";
    oss << qtd_estoque << ";" << fornecedor.getNome () << ";";

    oss << numero_lote << ";" << producao_lote << ";" << validade.To_string () << endl;

    return oss.str ();
}

/**
 * @details Faz a alteracao de qualquer dado da fruta escolhido pelo usuario
 */
void Fruta::AlteraFruta () {
    int opcao = -1;

    while (opcao != 0)
    {
        opcao = menu_alteracao_fruta ();

        if (opcao >= 1 && opcao <= 5)
        {
            AlteraProduto (opcao);
        }
        else if (opcao == 6)
        {
            cout << "Digite o novo numero do lote do produto: ";
            cin >> numero_lote;
        }
        else if (opcao == 7)
        {
            cout << "Digite a nova data de producao do lote (EX.: 12 05 2017): ";
            cin >> producao_lote;
        }
        else if (opcao < 0 || opcao > 7)
        {
            cout << "Opcao invalida !!!" << endl;
        }
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Fruta::~Fruta () {
    
}