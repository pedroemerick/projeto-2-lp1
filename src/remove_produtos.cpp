/**
* @file     remove_produtos.cpp
* @brief    Implementacao da funcao que remove um produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/05/2017
*/

#include "remove_produtos.h"
#include "funcoes_lista.h"
#include "produto.h"
#include "ll_dupla_ord.h"

#include <sstream>
using std::ws;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
    
/**
* @brief Função que remove um produto do programa atraves do nome do produto
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void remocao_produto (Lista <Produto*> &produtos, int &qtd_produtos)
{
    string nome_produto;

    // Recebe o nome do produto para remocao
    cout << "Digite o nome do produto que deseja remover: ";
    getline (cin >> ws, nome_produto);

    // Verifica se o produto existe 
    if (existe_produto (produtos, nome_produto) == true)
    {
        // Se existir é feita a remocao do produto da lista de produtos e a quantidade de produtos subtraida
        remove_produto_nome (produtos, nome_produto);
        qtd_produtos -= 1;

        cout << endl << "Produto removido com sucesso !!!" << endl;
    }
    else
    {
        cout << endl << "Produto não cadastrado !!!" << endl;
    }
}