/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
*/

#include "fornecedor.h"
#include "produto.h"
#include "cadastro_produtos.h"
#include "remove_produtos.h"
#include "funcoes_fornecedor.h"
#include "menus.h"
#include "alteracao_produto.h"
#include "arquivos.h"
#include "nota_fiscal.h"
#include "funcoes_cliente.h"
#include "ll_dupla_ord.h"
#include "consulta.h"

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std:: string;

#include <fstream>
using std::ofstream;
using std::ifstream;

/**
* @brief Função principal do programa.
*/
int main () {

    Lista <Fornecedor> fornecedores;
    int qtd_fornecedores = 0;

    ifstream arquivo_carrega_fornecedores ("./data/fornecedores.csv");
    if (arquivo_carrega_fornecedores) {
        carrega_fornecedores (arquivo_carrega_fornecedores, fornecedores, qtd_fornecedores);
        arquivo_carrega_fornecedores.close ();        
    }
    
    Lista <Produto*> produtos;
    int qtd_produtos = 0;

    ifstream arquivo_carrega_produtos ("./data/produtos.csv");
    if (arquivo_carrega_produtos) {
        carrega_produtos (arquivo_carrega_produtos, fornecedores, produtos, qtd_produtos);
        arquivo_carrega_produtos.close ();        
    }

    Nota_fiscal carrinho;

    int menu_p = -1;

    while (menu_p != 0)
    {   
        system("clear");
        menu_p = menu_principal ();
        system("clear");

        switch (menu_p) 
        {
            case 1:  {
                int menu_c = -1;

                while (menu_c != 0)
                {
                    menu_c = menu_comerciante ();
                    system("clear");

                    switch (menu_c) 
                    {
                        case 1:
                            cadastramento_produto (fornecedores, produtos, qtd_produtos);
                            break;
                        case 2:
                            remocao_produto (produtos, qtd_produtos);
                            break;
                        case 3:
                            cadastramento_fornecedor (fornecedores, qtd_fornecedores);
                            break;
                        case 4:
                            remocao_fornecedor (fornecedores, produtos, qtd_fornecedores);
                            break;
                        case 5:
                            alteracao_produto (produtos);
                            break;
                        case 6:
                            consulta_comerciante (produtos, fornecedores);
                            break;
                        case 0:
                            system("clear");
                            break;
                        default:
                            cout << "Opcao invalida !!!" << endl;
                            break;
                    }
                }
                break;
            }
            case 2: {
                
                int menu_f = -1;

                while (menu_f != 0) {

                    menu_f = menu_cliente ();
                    system("clear");

                    switch (menu_f) {
                        case 1:
                            consulta_cliente (produtos);
                            break;
                        case 2:
                            carrinho_compras (carrinho, produtos);
                            break;
                        case 0:
                            system("clear");
                            break;
                        default:
                            cout << "Opção inválida!" << endl;
                            break;
                    }
                }
                break;
            }
            case 0:
                system("clear");
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }

    ofstream arquivo_produtos ("./data/produtos.csv");
    arquivo_saida_produtos (arquivo_produtos, produtos, qtd_produtos);
    arquivo_produtos.close ();

    ofstream arquivo_fornecedores ("./data/fornecedores.csv");
    arquivo_saida_fornecedores (arquivo_fornecedores, fornecedores, qtd_fornecedores);
    arquivo_fornecedores.close ();

    cout << endl << endl << "Desenvolvido por Pedro Emerick e Valmir Correa." << endl;
    return 0;
}