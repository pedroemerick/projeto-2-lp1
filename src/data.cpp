/**
 * @file	data.cpp
 * @brief	Implementacao dos metodos da classe Data
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	05/06/2017
 * @sa		data.h
 */

#include "data.h"

#include <ostream>
using std::ostream;

#include <istream>
using std::istream;

#include <sstream>
using std::ostringstream;

/**
 * @details Os valores de dia, mes e ano sao inicializados com zero
 */
Data::Data () {
    dia = 0;
    mes = 0;
    ano = 0;
}

/**
 * @details Os valores dos dados da data sao recebidos por parametro
 * @param   d Dia para a data
 * @param   m Mes para a data
 * @param   a Ano para a data
 */
Data::Data (int d, int m, int a) {
    dia = d;
    mes = m;
    ano = a;
}

/**
 * @return Dia da data
 */
int Data::getDia () {
    return dia;
}

/**
 * @details O metodo modifica o dia da data
 * @param   d Dia para a data
 */
void Data::setDia (int d) {
    dia = d;
}

/**
 * @return Mes da data
 */
int Data::getMes () {
    return mes;
}

/**
 * @details O metodo modifica o mes da data
 * @param   m Mes para a data
 */
void Data::setMes (int m) {
    mes = m;
}

/**
 * @return Ano da data
 */
int Data::getAno () {
    return ano;
}

/**
 * @details O metodo modifica o ano da data
 * @param   a Ano para a data
 */
void Data::setAno (int a){
    ano = a;
}

/** 
 * @details O operador e sobrecarregado para representar uma Data
 *			com os dados da Data
 * @param	os Referencia para stream de saida
 * @param	d Referencia para o objeto Data a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream& os, const Data& d) {
    return os << d.dia << "/" << d.mes << "/" << d.ano;
}

/** 
 * @details O operador e sobrecarregado para receber os dados uma Data
 * @param	i Referencia para stream de entrada
 * @param	d Referencia para o objeto Data a receber os dados
 * @return	Referencia para stream de entrada
 */
istream& operator >> (istream& i, Data &d) {
    i >> d.dia >> d.mes >> d.ano;
    return i;
}

/** 
 * @details O metodo coloca os dados da data em uma string,
 *          separados por '/'
 * @return	String com os dados da data
 */
string Data::To_string () {
    ostringstream oss;

    oss << dia << "/" << mes << "/" << ano;

    return oss.str ();
}