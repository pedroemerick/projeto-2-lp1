/**
 * @file	produto.cpp
 * @brief	Implementacao dos metodos da classe Produto
 * @author	Pedro Emerick (p.emerick@live.com)
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	30/05/2017
 * @date	05/06/2017
 * @sa		produto.h
 */

#include "produto.h"
#include "fornecedor.h"

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include <sstream>
using std::ws;

/**
 * @details Os valores de tipo e descricao sao inicializados com um espaco 
 *          e os valores de codigo, preco e quantidade em estoque sao inicializados com zero
 */
Produto::Produto () {
    tipo = " ";
    codigo = 0;
    descricao = " ";
    preco = 0;
    qtd_estoque = 0;
}

/**
 * @return Tipo do produto
 */
string Produto::getTipo () {
    return tipo;
}

/**
 * @return Nome do produto
 */
string Produto::getNome () {
    return nome;
}

/**
 * @return Codigo do produto
 */
double Produto::getCodigo () {
    return codigo;
}

/**
 * @return Descricao do produto
 */
string Produto::getDescricao () {
    return descricao;
}

/**
 * @return Preco do produto
 */
float Produto::getPreco () {
    return preco;
}

/**
 * @return Quantidade em estoque do produto
 */
int Produto::getQtd_estoque () {
    return qtd_estoque;
}

/**
 * @details O metodo modifica a quantidade em estoque do produto
 * @param   qe Quantidade em estoque para o produto
 */
void Produto::setQtd_estoque (int qe) {
    qtd_estoque = qe;
}

/**
 * @return Fornecedor do produto
 */
Fornecedor Produto::getFornecedor () {
    return fornecedor;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao se é menor
 *			um instante de Produto a outro
 * @param	p Instante de Produto que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é menor que o outro
 */
bool Produto::operator < (const Produto &p) {
    if (nome < p.nome)
        return true;
    
    return false;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de Produto a outro
 * @param	p Instante de Produto que sera atribuido ao objeto que invoca o metodo
 * @return	Se um instante é igual ao outro
 */
bool Produto::operator == (const Produto &p) {
    if (nome == p.nome) {
        return true;
    }

    return false;
}

/** 
 * @details O operador e sobrecarregado para representar um Produto
 *			com os dados do produto
 * @param	os Referencia para stream de saida
 * @param	p Referencia para o objeto Produto a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream& os, const Produto& p) {
    return p.print(os);
}

/**
 * @details Faz a alteracao de qualquer dado do produto escolhido pelo usuario
 */
void Produto::AlteraProduto (int opcao) {

    switch (opcao) 
    {
        case 1: {       // NOME
            cout << "Digite o novo nome para o produto: ";
            getline (cin >> ws, nome);
            break;
        }
        case 2: {       // CODIGO
            cout << "Digite o novo codigo para o produto: ";
            cin >> codigo;
            break;
        }
        case 3: {       // DESCRICAO
            cout << "Digite a nova descricao para o produto: ";
            getline (cin >> ws, descricao);
            break;
        }
        case 4: {       // PRECO
            cout << "Digite o novo preco para o produto: ";
            cin >> preco;
            break;
        }
        case 5: {       // QUANTIDADE EM ESTOQUE
            cout << "Digite a nova quantidade em estoque do produto: ";
            cin >> qtd_estoque;
            break;
        }
        default:
            cout << "Opcao invalida !!!" << endl;
            break;
    }
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Produto::~Produto () {

}