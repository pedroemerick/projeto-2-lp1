#Makefile for "Projeto - 02 - LP1" C++ application.
#Created by Valmir Correa and Pedro Emerick 30/05/2017.

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
DAT_DIR=./data
TEST_DIR=./test

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxygen doc

# Define o alvo para a compilação completa:
all: QLeveTudo

# Debug:
debug: CFLAGS += -g -O0
debug: clean QLeveTudo


# Alvo para a contrução do executável 'QLeveTudo':
QLeveTudo: $(OBJ_DIR)/main.o $(OBJ_DIR)/bebida.o $(OBJ_DIR)/data.o $(OBJ_DIR)/doce.o $(OBJ_DIR)/fruta.o $(OBJ_DIR)/produto.o $(OBJ_DIR)/salgado.o $(OBJ_DIR)/livro.o $(OBJ_DIR)/dvd.o $(OBJ_DIR)/cd.o $(OBJ_DIR)/produto_perecivel.o $(OBJ_DIR)/fornecedor.o $(OBJ_DIR)/menus.o $(OBJ_DIR)/cadastro_produtos.o $(OBJ_DIR)/remove_produtos.o $(OBJ_DIR)/funcoes_fornecedor.o $(OBJ_DIR)/consulta.o $(OBJ_DIR)/funcoes_lista.o $(OBJ_DIR)/arquivos.o $(OBJ_DIR)/nota_fiscal.o $(OBJ_DIR)/item_produto.o $(OBJ_DIR)/funcoes_cliente.o $(OBJ_DIR)/alteracao_produto.o
#$(OBJ_DIR)/alteracao_produto.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel QLeveTudo criado em $(BIN_DIR)] ***"
	@echo "====================================================="

# Alvo para a construção do 'main.o':
$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/fornecedor.h $(INC_DIR)/produto.h $(INC_DIR)/cadastro_produtos.h $(INC_DIR)/remove_produtos.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/funcoes_fornecedor.h $(INC_DIR)/menus.h $(INC_DIR)/alteracao_produto.h $(INC_DIR)/consulta.h $(INC_DIR)/arquivos.h $(INC_DIR)/nota_fiscal.h $(INC_DIR)/funcoes_cliente.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'bebida.o':
$(OBJ_DIR)/bebida.o: $(SRC_DIR)/bebida.cpp $(INC_DIR)/bebida.h $(INC_DIR)/produto.h $(INC_DIR)/produto_perecivel.h $(INC_DIR)/data.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'data.o':
$(OBJ_DIR)/data.o: $(SRC_DIR)/data.cpp $(INC_DIR)/data.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'doce.o':
$(OBJ_DIR)/doce.o: $(SRC_DIR)/doce.cpp $(INC_DIR)/doce.h $(INC_DIR)/produto.h $(INC_DIR)/produto_perecivel.h $(INC_DIR)/data.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'fruta.o':
$(OBJ_DIR)/fruta.o: $(SRC_DIR)/fruta.cpp $(INC_DIR)/fruta.h $(INC_DIR)/produto.h $(INC_DIR)/produto_perecivel.h $(INC_DIR)/data.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'produto.o':
$(OBJ_DIR)/produto.o: $(SRC_DIR)/produto.cpp $(INC_DIR)/produto.h $(INC_DIR)/fornecedor.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'salgado.o':
$(OBJ_DIR)/salgado.o: $(SRC_DIR)/salgado.cpp $(INC_DIR)/salgado.h $(INC_DIR)/produto.h $(INC_DIR)/produto_perecivel.h $(INC_DIR)/data.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'livro.o':
$(OBJ_DIR)/livro.o: $(SRC_DIR)/livro.cpp $(INC_DIR)/livro.h $(INC_DIR)/produto.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'dvd.o':
$(OBJ_DIR)/dvd.o: $(SRC_DIR)/dvd.cpp $(INC_DIR)/dvd.h $(INC_DIR)/produto.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'cd.o':
$(OBJ_DIR)/cd.o: $(SRC_DIR)/cd.cpp $(INC_DIR)/cd.h $(INC_DIR)/produto.h $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'produto_perecivel.o':
$(OBJ_DIR)/produto_perecivel.o: $(SRC_DIR)/produto_perecivel.cpp $(INC_DIR)/produto_perecivel.h $(INC_DIR)/data.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'fornecedor.o':
$(OBJ_DIR)/fornecedor.o: $(SRC_DIR)/fornecedor.cpp $(INC_DIR)/fornecedor.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'menus.o':
$(OBJ_DIR)/menus.o: $(SRC_DIR)/menus.cpp $(INC_DIR)/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'cadastro_produtos.o':
$(OBJ_DIR)/cadastro_produtos.o: $(SRC_DIR)/cadastro_produtos.cpp $(INC_DIR)/cadastro_produtos.h $(INC_DIR)/menus.h $(INC_DIR)/fornecedor.h $(INC_DIR)/cd.h $(INC_DIR)/dvd.h $(INC_DIR)/livro.h $(INC_DIR)/salgado.h $(INC_DIR)/fruta.h $(INC_DIR)/doce.h $(INC_DIR)/bebida.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/funcoes_lista.h	
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'remove_produtos.o':
$(OBJ_DIR)/remove_produtos.o: $(SRC_DIR)/remove_produtos.cpp $(INC_DIR)/remove_produtos.h $(INC_DIR)/menus.h $(INC_DIR)/fornecedor.h $(INC_DIR)/cd.h $(INC_DIR)/dvd.h $(INC_DIR)/livro.h $(INC_DIR)/salgado.h $(INC_DIR)/fruta.h $(INC_DIR)/doce.h $(INC_DIR)/bebida.h $(INC_DIR)/ll_dupla_ord.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'funcoes_fornecedor.o':
$(OBJ_DIR)/funcoes_fornecedor.o: $(SRC_DIR)/funcoes_fornecedor.cpp $(INC_DIR)/funcoes_fornecedor.h $(INC_DIR)/fornecedor.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/produto.h $(INC_DIR)/ll_dupla_ord.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'consulta.o':
$(OBJ_DIR)/consulta.o: $(SRC_DIR)/consulta.cpp $(INC_DIR)/consulta.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/produto.h $(INC_DIR)/menus.h $(INC_DIR)/nota_fiscal.h $(INC_DIR)/item_produto.h $(INC_DIR)/funcoes_lista.h $(INC_DIR)/fornecedor.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'funcoes_lista.o':
$(OBJ_DIR)/funcoes_lista.o: $(SRC_DIR)/funcoes_lista.cpp $(INC_DIR)/funcoes_lista.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/produto.h $(INC_DIR)/fornecedor.h $(INC_DIR)/item_produto.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'arquivos.o':
$(OBJ_DIR)/arquivos.o: $(SRC_DIR)/arquivos.cpp $(INC_DIR)/arquivos.h $(INC_DIR)/produto.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/fornecedor.h $(INC_DIR)/cd.h $(INC_DIR)/dvd.h $(INC_DIR)/livro.h $(INC_DIR)/salgado.h $(INC_DIR)/fruta.h $(INC_DIR)/doce.h $(INC_DIR)/bebida.h $(INC_DIR)/funcoes_lista.h $(INC_DIR)/nota_fiscal.h $(INC_DIR)/consulta.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'alteracao_produto.o':
$(OBJ_DIR)/alteracao_produto.o: $(SRC_DIR)/alteracao_produto.cpp $(INC_DIR)/alteracao_produto.h $(INC_DIR)/cd.h $(INC_DIR)/dvd.h $(INC_DIR)/livro.h $(INC_DIR)/salgado.h $(INC_DIR)/fruta.h $(INC_DIR)/doce.h $(INC_DIR)/bebida.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/produto.h $(INC_DIR)/funcoes_lista.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'item_produto.o':
$(OBJ_DIR)/item_produto.o: $(SRC_DIR)/item_produto.cpp $(INC_DIR)/item_produto.h $(INC_DIR)/produto.h 
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'nota_fiscal.o':
$(OBJ_DIR)/nota_fiscal.o: $(SRC_DIR)/nota_fiscal.cpp $(INC_DIR)/nota_fiscal.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/item_produto.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do 'funces_cliente.o':
$(OBJ_DIR)/funcoes_cliente.o: $(SRC_DIR)/funcoes_cliente.cpp $(INC_DIR)/funcoes_cliente.h $(INC_DIR)/ll_dupla_ord.h $(INC_DIR)/nota_fiscal.h $(INC_DIR)/menus.h $(INC_DIR)/consulta.h 
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes ./bin/QLeveTudo


# Alvo para a geração automatica de documentação usando o Doxygen:
doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile


# Alvo usado para limpar os arquivos temporários:
clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
