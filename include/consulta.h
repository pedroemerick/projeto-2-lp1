/**
* @file     consulta.h
* @brief    Declaracao dos prototipos das funcoes que mostram os dados de um produto ou fornecedor de acordo
*           com a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef CONSULTA_H
#define CONSULTA_H

#include "produto.h"
#include "ll_dupla_ord.h"
#include "nota_fiscal.h"

/**
* @brief Função que mostra as opcoes de consulta do cliente e imprime o resultado da consulta desejada
* @param produtos Lista de produtos
*/
void consulta_cliente (Lista <Produto*> &produtos);

/**
* @brief Função que mostra as opcoes de consulta por tipo, recebe a opcao do usuario 
*        e mostra os produtos do tipo desejado
* @param produtos Lista de produtos
*/
void consulta_tipos (Lista <Produto*> &produtos);

/**
* @brief Função que imprime os produtos de acordo com o tipo desejado
* @param produtos Lista de produtos
* @param tipo Tipo desejado para impressao dos produtos
*/
void mostrar_produto_tipo (Lista <Produto*> &produtos, string tipo);

/**
* @brief Função que imprime os produtos de acordo com o nome do produto
* @param produtos Lista de produtos
* @param nome Nome do produto para impressao
*/
void mostrar_produto_nome (Lista <Produto*> &produtos, string nome);

/**
* @brief Função que mostra as opcoes de consulta do comerciante e imprime o resultado da consulta desejada
* @param produtos Lista de produtos
* @param fornecedores Lista de fornecedores
*/
void consulta_comerciante (Lista <Produto*> &produtos, Lista <Fornecedor> &fornecedores);

/**
* @brief Função que imprime os produtos de acordo com o fornecedor
* @param produtos Lista de produtos
* @param fornecedor Fornecedor para consulta
*/
void mostrar_produto_fornecedor (Lista <Produto*> &produtos, Fornecedor &fornecedor);

/**
* @brief Função que imprime o fornecedor de acordo com o nome
* @param fornecedores Lista de fornecedores
* @param nome Nome para impressao do fornecedor
*/
void mostrar_fornecedor_nome (Lista <Fornecedor> &fornecedores, string nome);

/**
* @brief Função que imprime os produtos que estão no carrinho de compras.
* @param &carrinho Referêcia ao objeto carrinho.
*/
void mostrar_carrinho (Nota_fiscal &carrinho);

/**
* @brief Função que imprime o estoque de produtos.
* @param &produtos Referêcia a lista de ponteiros de produtos.
*/
void mostrar_estoque (Lista <Produto*> &produtos);

/**
* @brief Função que verifica a quantidade de itens no carriho de compras.
* @param &carrinho Referêcia ao objeto carrinho.
*/
bool consultar_qtd_itens_carrinho (Nota_fiscal &carrinho);

/**
* @brief Função que imprime as informações necessários para contruir a nota fiscal.
* @param &carrinho Referêcia ao objeto carrinho.
*/
void produtos_cupom_fiscal (Nota_fiscal &carrinho);
#endif