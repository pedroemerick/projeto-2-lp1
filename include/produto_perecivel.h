/**
* @file   produto_perecivel.h
* @brief  Arquivo com os atributos e metodos da classe Perecivel
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   05/06/2017
*/

#ifndef PRODUTO_PERECIVEL_H_
#define PRODUTO_PERECIVEL_H_

#include "data.h"

/**
 * @class   Perecivel produto_perecivel.h
 * @brief   Classe que representa um produto perecivel
 * @details Os atributos de um produto perecivel é a data de validade
 */ 
class Perecivel {
    protected:
        Data validade;          /**< Data de validade do produto */
    public:
        /** @brief Construtor padrao */
        Perecivel ();

        /** @brief Construtor parametrizado */
        Perecivel (Data v);

        /** @brief Retorna se um produto esta ou nao fora da data de validade */
        bool ConsumoProduto (); 
        
        /** @brief Destrutor padrao */
        ~Perecivel ();
};

#endif