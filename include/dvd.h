/**
* @file   dvd.h
* @brief  Arquivo com os atributos e metodos da classe DVD
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#include <string>
using std::string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include "produto.h"

#ifndef DVD_H
#define DVD_H

/**
 * @class   DVD dvd.h
 * @brief   Classe que representa um dvd
 * @details Os atributos de um dvd são o titulo, genero e a duracao em minutos
 */ 
class DVD : public Produto {
    private:
        string titulo;                      /**< Titulo do dvd */
        string genero;                      /**< Genero do dvd */
        int duracao;                        /**< Duracao (em minutos) do dvd */
        ostream& print (ostream &os) const {

            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Titulo: " << titulo << endl;
            os << "Genero: " << genero << endl;
            os << "Duracao: " << duracao << " min" << endl;

            return os;    
        }
    public:
        /** @brief Construtor padrao */
        DVD ();

        /** @brief Construtor parametrizado */
        DVD (string n, double c, string d, float p, int qe, string t, string g, int dt, Fornecedor &f);

        /** @brief Retorna o titulo do dvd */
        string getTitulo ();

        /** @brief Modifica o titulo do dvd */
        void setTitulo (string t);

        /** @brief Retorna o genero do dvd */
        string getGenero ();

        /** @brief Modifica o genero do dvd */
        void setGenero (string g);

        /** @brief Retorna a duracao do dvd */
        int getDuracao ();

        /** @brief Modifica a duracao do dvd */
        void setDuracao (int d);

        /** @brief Retorna uma string com todos os dados do dvd */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraDVD ();

        /** @brief Destrutor padrao */
        ~DVD ();
};

#endif