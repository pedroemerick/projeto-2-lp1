/**
* @file   bebida.h
* @brief  Arquivo com os atributos e metodos da classe Bebida
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#ifndef BEBIDA_H_
#define BEBIDA_H_

#include "produto.h"
#include "produto_perecivel.h"
#include "fornecedor.h"
#include "data.h"

#include <iostream>
using std::endl;

#include <ostream>
using std::ostream;

/**
 * @class   Bebida bebida.h
 * @brief   Classe que representa uma bebida
 * @details Os atributos de uma bebida são o teor alcoolico e a quantidade de acucar
 */ 
class Bebida : public Produto, public Perecivel {
    private:
        float teor_alcoolico;                   /**< Porcentagem de teor alcoolico da bebida */
        float qtd_acucar;                       /**< Quantidade de acucar em miligramas da bebida */
        ostream& print (ostream &os) const {
            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Teor Alcoolico: " << teor_alcoolico << " %" << endl;
            os << "Quantidade de acucar: " << qtd_acucar << " mg" << endl;
            os << "Validade: " << validade << endl;

            return os;    
        }
    public:
        /** @brief Construtor padrao */
        Bebida ();
        
        /** @brief Construtor parametrizado */
        Bebida (string n, double c, string d, float p, int qe, float t, float q, Fornecedor &f, Data v);

        /** @brief Retorna a porcentagem de teor alcoolico da bebida */
        float getTeor_alcoolico ();

        /** @brief Modifica a porcentagem de teor alcoolico da bebida */
        void setTeor_alcoolico (float ta);

        /** @brief Retorna a quantidade de acucar da bebida */        
        float getQtd_acucar ();

        /** @brief Modifica a quantidade de acucar da bebida */                
        void setQtd_acucar (float qa);

        /** @brief Retorna uma string com todos os dados da bebida */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraBebida ();
        
        /** @brief Destrutor padrao */
        ~Bebida ();
};

#endif