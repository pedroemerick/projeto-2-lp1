/**
* @file     funcoes_lista.h
* @brief    Declaracao dos prototipos das funcoes que manipulam as listas de produtos e fornecedores
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef FUNCOES_LISTA_H
#define FUNCOES_LISTA_H

#include "fornecedor.h"
#include "ll_dupla_ord.h"
#include "produto.h"
#include "item_produto.h"

/**
* @brief Função que verifica se existe um fornecedor cadastrado de acordo com o nome recebido
* @param fornecedores Lista de fornecedores
* @param nome_fornecedor Nome do fornecedor para verificacao
* @return Se o fornecedor existe ou nao
*/
bool existe_fornecedor (Lista <Fornecedor> &fornecedores, string nome_fornecedor);

/**
* @brief Função que busca um fornecedor atraves do nome e o retorna com todos os seus dados se encontrado
* @param fornecedores Lista de fornecedores
* @param nome_fornecedor Nome do fornecedor para retorno
* @return Fornecedor buscado pelo nome
*/
Fornecedor* retorna_fornecedor (Lista <Fornecedor> &fornecedores, string nome_fornecedor);

/**
* @brief Função que remove um fornecedor da lista atraves do seu nome
* @param fornecedores Lista de fornecedores
* @param nome_fornecedor Nome do fornecedor para remocao
*/
void remove_fornecedor_nome (Lista <Fornecedor> &fornecedores, string nome_fornecedor);

/**
* @brief Função que verifica se existe um produto cadastrado de acordo com o nome recebido
* @param produtos Lista de pprodutos
* @param nome_produto Nome do produto para verificacao
* @return Se o produto existe ou nao
*/
bool existe_produto (Lista <Produto*> &produtos, string nome_produto);

/**
* @brief Função que busca um produto atraves do nome e o retorna com todos os seus dados se encontrado
* @param produtos Lista de produtos
* @param nome_produto Nome do produto para retorno
* @return Produto buscado pelo nome
*/
Produto* retorna_produto (Lista <Produto*> &produtos, string nome_produto);

/**
* @brief Função que remove um produto da lista atraves do seu nome
* @param produtos Lista de produtos
* @param nome_produto Nome do produto para remocao
*/
void remove_produto_nome (Lista <Produto*> &produtos, string nome_produto);

/**
* @brief Função que verifica se existe algum produto fornecido por um determinado fornecedor
* @param produtos Lista de produtos
* @param fornecedor Fornecedor para verificacao
* @return Se existe ou nao algum produto fornecido pelo determinado fornecedor
*/
bool verifica_fornecedor (Lista <Produto*> &produtos, Fornecedor &fornecedor);

/**
* @brief Função que busca um item atraves do nome e o retorna com todos os seus dados, se encontrado.
* @param produtos Lista de itens de produtos.
* @param nome_produto Nome do produto para retorno.
* @return Item buscado pelo nome.
*/
Item_produto* retorna_item (Lista <Item_produto> &produtos, string nome_produto);

/**
* @brief Função que remove todos os produtos do carrinho de compras
* @param carrinho Lista de itens do carrinho de compras
*/
void limpar_carrinho (Lista <Item_produto> &produtos);

#endif