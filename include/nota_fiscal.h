/**
* @file	    nota_fiscal.h
* @brief	Arquivo com os atributos e metodos da classe Nota_fiscal
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
*/

#ifndef NOTA_FISCAL_H_
#define NOTA_FISCAL_H_

#include "ll_dupla_ord.h"
#include "item_produto.h"

/**
 * @class   Nota_fiscal
 * @brief   Classe que representa uma nota fiscal.
 */
class Nota_fiscal {
    private:
        int numero;                              /** < Numero da nota fiscal */
        Lista <Item_produto> itens_produtos;     /** < Lista com os produtos do cliente */
        float total;                             /** < Valor total da compra */
    public:
        /** @brief Construtor padrao */
        Nota_fiscal ();

        /** @brief Retorna o total da compra */
        float getTotal ();

        /** @brief Retorna o numero da nota fiscal */
        int getNumero ();

        /** @brief Retorna uma lista de itens de produtos */
        Lista <Item_produto>* RetornaItens ();

        /** @brief Adiciona um item na lista de itens */
        void AddItem (Item_produto &p);

        /** @brief Remove um item da lista, pelo nome */
        void RemoverItem (string nome);

        /** @brief Faz as devidas operacoes para a finalizacao da compra */
        void FinalizarCompra ();

        /** @brief Destrutor padrao */
        ~Nota_fiscal ();
};

#endif