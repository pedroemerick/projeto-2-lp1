/**
* @file   fornecedor.h
* @brief  Arquivo com os atributos e metodos da classe Fornecedor
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   05/06/2017
*/

#ifndef FORNECEDOR_H_
#define FORNECEDOR_H_

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

/**
 * @class   Fornecedor fornecedor.h
 * @brief   Classe que representa um fornecedor
 * @details Os atributos de um fornecedor são o nome e o cnpj
 */ 
class Fornecedor {
    private:
        string nome;                    /**< Nome do fornecedor */
        long int cnpj;                  /**< CNPJ do fornecedor */
    public:
        /** @brief Construtor padrao */
        Fornecedor ();

        /** @brief Construtor parametrizado */
        Fornecedor (string n, long int c);

        /** @brief Modifica o nome do fornecedor */
        void setNome (string n);

        /** @brief Retorna o nome do fornecedor */
        string getNome ();

        /** @brief Sobrecarga do operador menor */
        bool operator < (Fornecedor const f);

        /** @brief Sobrecarga do operador de igualdade */
        bool operator == (Fornecedor const f);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &o, Fornecedor const &f);

        /** @brief Retorna uma string com todos os dados do fornecedor */
        string To_string ();

        /** @brief Destrutor padrao */
        ~Fornecedor ();
}; 

#endif