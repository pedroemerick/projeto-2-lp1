/**
* @file   data.h
* @brief  Arquivo com os atributos e metodos da classe Data
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   05/06/2017
*/

#ifndef DATA_H_
#define DATA_H_

#include <ostream>
using std::ostream;

#include <istream>
using std::istream;

#include <string>
using std::string;

/**
 * @class   Data data.h
 * @brief   Classe que representa uma data
 * @details Os atributos de uma data são o dia, mes e ano
 */ 
class Data {
    private:
        int dia;                /**< Dia da data */
        int mes;                /**< Mes da data */
        int ano;                /**< Ano da data */
    public:
        /** @brief Construtor padrao */
        Data ();

        /** @brief Construtor parametrizado */
        Data (int d, int m, int a);

        /** @brief Retorna o dia da data */
        int getDia ();

        /** @brief Modifica o dia da data */
        void setDia (int d);

        /** @brief Retorna o mes da data */
        int getMes ();

        /** @brief Modifica o mes da data */
        void setMes (int m);

        /** @brief Retorna o ano da data */
        int getAno ();

        /** @brief Modifica o ano da data */
        void setAno (int a);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream& os, const Data& d);

        /** @brief Sobrecarga do operador de extracao em stream */
        friend istream& operator >> (istream& i, Data &d);

        /** @brief Retorna uma string com todos os dados da data */
        string To_string ();
};

#endif