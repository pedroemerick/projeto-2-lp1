/**
* @file     arquivos.h
* @brief    Declaracao dos prototipos das funcoes que manipulam os arquivos de entrada e saida de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef ARQUIVOS_H
#define ARQUIVOS_H

#include "ll_dupla_ord.h"
#include "produto.h"
#include "fornecedor.h"
#include "nota_fiscal.h"

#include <fstream>
using std::ofstream;
using std::ifstream;

/**
* @brief Função que salva os produtos em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param produtos Lista com todos produtos cadastrados
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void arquivo_saida_produtos (ofstream &arquivo, Lista <Produto*> &produtos, int &qtd_produtos);

/**
* @brief Função que salva os fornecedores em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param fornecedores Lista com todos fornecedores cadastrados
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void arquivo_saida_fornecedores (ofstream &arquivo, Lista <Fornecedor> &fornecedores, int &qtd_fornecedores);

/**
* @brief Função que carrega os fornecedores de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param fornecedores Lista de fornecedores
* @param qtd_fornecedores Quantidade de fornecedores
*/
void carrega_fornecedores (ifstream &arquivo, Lista <Fornecedor> &fornecedores, int &qtd_fornecedores);

/**
* @brief Função que carrega os produtos de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos
*/
void carrega_produtos (ifstream &arquivo, Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_produtos);

/**
* @brief Função que gera a nota fiscal em um arquivo txt para o cliente
* @param arquivo Arquivo para leitura dos dados
* @param carrinho Carrinho de compras
*/
void saida_nota_fiscal (ofstream &arquivo, Nota_fiscal &carrinho);

#endif