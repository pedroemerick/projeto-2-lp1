/**
* @file	    funcoes_cliente.h
* @brief	Arquivo com os cabeçarios referentes as manipulações dos produtos feitas pelo cliente.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
*/

#ifndef FUNCOES_CLIENTE_H_
#define FUNCOES_CLIENTE_H_

#include "ll_dupla_ord.h"
#include "funcoes_cliente.h"
#include "nota_fiscal.h"

/**
* @brief Função que imprime o menu do carrinho de comprar do cliente.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void carrinho_compras (Nota_fiscal &carrinho, Lista <Produto*> &produtos );

/**
* @brief Função que gera a nota fiscal com todos os dados da compra.
* @param carrinho Objeto que contem os dados da compra.
*/
void finalizar_compra (Nota_fiscal &carrinho);

/**
* @brief Função que insere no carrinho o item escolhido pelo usuário.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void inserir_produtos (Nota_fiscal &carrinho, Lista <Produto*> &produtos );

/**
* @brief Função que remove um item do carrinho de compras.
* @param carrinho Objeto que contem os dados da compra.
* @param produtos Lista de produtos do estoque.
*/
void remover_produtos (Nota_fiscal &carrinho, Lista <Produto*> &produtos);

#endif