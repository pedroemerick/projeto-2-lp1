/**
* @file   fruta.h
* @brief  Arquivo com os atributos e metodos da classe Fruta
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#ifndef FRUTA_H_
#define FRUTA_H_

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include <string>
using std::string;

#include "data.h"
#include "produto.h"
#include "produto_perecivel.h"

/**
 * @class   Fruta fruta.h
 * @brief   Classe que representa uma fruta
 * @details Os atributos de uma fruta são o numero do lote e a data de producao do lote
 */ 
class Fruta : public Produto, public Perecivel {
    private:
        int numero_lote;                    /**< Numero do lote da fruta */
        Data producao_lote;                 /**< Data de produção do lote */
        ostream& print (ostream &os) const {

            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Numero do lote: " << numero_lote << endl;
            os << "Producao do lote: " << producao_lote << endl;
            os << "Validade: " << validade << endl;

            return os;    
        }
    public:
        /** @brief Construtor padrao */
        Fruta ();

        /** @brief Construtor parametrizado */
        Fruta (string n, double c, string d, float p, int qe, int nl, Data pl, Fornecedor &f, Data v);

        /** @brief Retorna o numero do lote da fruta */
        int getNumero_lote ();

        /** @brief Modifica o numero do lote da fruta */
        void setNumero_lote (int nl);
        
        /** @brief Retorna a data de producao do lote da fruta */
        Data getProducao_lote ();

        /** @brief Modifica a data de producao do lote da fruta */
        void setProducao_lote (Data pl);

        /** @brief Retorna uma string com todos os dados da fruta */
        string To_string ();
        
        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraFruta ();
        
        /** @brief Destrutor padrao */
        ~Fruta ();
};

#endif