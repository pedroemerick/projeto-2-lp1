/**
* @file   produto.h
* @brief  Arquivo com os atributos e metodos da classe Produto
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   05/06/2017
*/

#ifndef PRODUTO_H_
#define PRODUTO_H_

#include "produto.h"
#include "fornecedor.h"

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

/**
 * @class   Produto produto.h
 * @brief   Classe que representa um produto
 * @details Os atributos de um produto são o tipo, o nome, o codigo, a descricao, o preco, 
 *          a quantidade em estoque e o fornecedor
 */
class Produto {
    private:
        virtual ostream& print(ostream&) const = 0;
    protected:
        string tipo;                        /**< Tipo do produto */
        string nome;                        /**< Nome do produto */
        double codigo;                      /**< Codigo do produto */
        string descricao;                   /**< Descricao do produto */
        float preco;                        /**< Preco do produto */
        int qtd_estoque;                    /**< Quantidade em estoque do produto */
        Fornecedor fornecedor;              /**< Fornecedor do produto */
    public:
        /** @brief Construtor padrao */
        Produto ();

        /** @brief Retorna o tipo do produto */
        string getTipo ();

        /** @brief Retorna o nome do produto */
        string getNome ();

        /** @brief Retorna o codigo do produto */
        double getCodigo ();

        /** @brief Retorna a descricao do produto */
        string getDescricao ();

        /** @brief Retorna o preco do produto */
        float getPreco ();

        /** @brief Retorna a quantidade em estoque do produto */
        int getQtd_estoque ();

        /** @brief Modifica a quantidade em estoque do produto */
        void setQtd_estoque (int qe);

        /** @brief Retorna o fornecedor do produto */
        Fornecedor getFornecedor ();

        /** @brief Sobrecarga do operador menor */
        bool operator < (const Produto &p);

        /** @brief Sobrecarga do operador de igualdade */
        bool operator == (const Produto &p);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream& os, const Produto& p);

        /** @brief Metodo virutal puro que retorna uma string com todos os dados do produto */
        virtual string To_string () = 0;

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraProduto (int opcao);

        /** @brief Destrutor padrao */
        ~Produto ();
};

#endif