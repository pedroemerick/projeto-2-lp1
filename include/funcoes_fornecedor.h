/**
* @file     funcoes_fornecedor.h
* @brief    Declaracao dos prototipos das funcoes que cadastram e removem um fornecedor
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef FUNCOES_FORNECEDOR_H
#define FUNCOES_FORNECEDOR_H

#include "ll_dupla_ord.h"
#include "fornecedor.h"
#include "produto.h"

/**
* @brief Função que recebe os dados necessarios de um fornecedor e cadastra no programa 
*        se nao houver outro fornecedor com o mesmo nome
* @param fornecedores Lista de fornecedores
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void cadastramento_fornecedor (Lista <Fornecedor> &fornecedores, int &qtd_fornecedores);

/**
* @brief Função que recebe o nome de um fornecedor e o remove do programa se nao houver
*        nenhum produto já cadastrado com ele fornecendo
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_fornecedores Quantidade de fornecedores cadastrados
*/
void remocao_fornecedor (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_fornecedores);

#endif