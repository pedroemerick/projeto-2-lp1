/**
* @file	    item_produto.h
* @brief	Arquivo com os atributos e metodos da classe item_produto
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	30/05/1017
* @date	    06/06/1017
*/

#ifndef ITEM_PRODUTO_H_
#define ITEM_PRODUTO_H_

#include <ostream>
using std::ostream;

#include "produto.h"

/**
 * @class   Item_produto 
 * @brief   Classe que representa um Item
 * @details Os atributos de um Item são do tipo: Ponteiro para Produto e quantidade de itens.
 */
class Item_produto
{
  private:
    Produto *item_p_produto;   /** < Lista com os produtos do cliente */
    int qtd_itens;             /** < Valor total da compra */
  public:
    /** @brief Construtor padrao */
    Item_produto();

    /** @brief Construtor parametrizado */
    Item_produto(Produto &p, int qtd);

    /** @brief Retorna um produto */
    Produto *getProduto();

    /** @brief Retorna quantidade de produtos */
    int getQtd_produtos();

    /** @brief Modifica a quantidade de produtos */
    void setQtd_produtos(int q);

    /** @brief Sobrecarga do operador de insercao em stream */
    friend ostream& operator<<(ostream& os, const Item_produto &i);
    
    /** @brief Sobrecarga do operador menor */
    bool operator<(const Item_produto &i);

    /** @brief Sobrecarga do operador de igualdade */
    bool operator==(const Item_produto &i);

    /** @brief Destrutor padrao */
    ~Item_produto();
};

#endif