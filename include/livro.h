/**
* @file   livro.h
* @brief  Arquivo com os atributos e metodos da classe Livro
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#include <string>
using std::string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include "produto.h"

#ifndef LIVRO_H
#define LIVRO_H

/**
 * @class   Livro livro.h
 * @brief   Classe que representa um Livro
 * @details Os atributos de um livro são o titulo, autor, editora e ano de publicacao
 */ 
class Livro : public Produto {
    private:
        string titulo;                      /**< Titulo do livro */
        string autor;                       /**< Autor do livro */
        string editora;                     /**< Editora do livro */
        int ano_publicacao;                 /**< Ano de publicacao do livro */
        ostream& print (ostream &os) const {

            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Titulo: " << titulo << endl;
            os << "Autor: " << autor << endl;
            os << "Editora: " << editora << endl;
            os << "Ano de publicacao: " << ano_publicacao << endl;

            return os;    
        }
    public:
        /** @brief Construtor padrao */
        Livro ();

        /** @brief Construtor parametrizado */
        Livro (string n, double c, string d, float p, int qe, string t, string a, string e, int ap, Fornecedor &f);

        /** @brief Retorna o titulo do livro */
        string getTitulo ();
        
        /** @brief Modifica o titulo do livro */        
        void setTitulo (string t);

        /** @brief Retorna o autor do livro */        
        string getAutor ();

        /** @brief Modifica o autor do livro */
        void setAutor (string a);

        /** @brief Retorna a editora do livro */
        string getEditora ();

        /** @brief Modifica a editora do livro */
        void setEditora (string e);

        /** @brief Retorna o ano de publicacao do livro */
        int getAno_publicacao ();

        /** @brief Modifica o ano de publicacao do livro */
        void setAno_publicacao (int ap);

        /** @brief Retorna uma string com todos os dados do livro */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraLivro ();

        /** @brief Destrutor padrao */
        ~Livro ();
};

#endif