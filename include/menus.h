/**
* @file     menus.h
* @brief    Declaracao dos prototipos das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    05/06/2017
*/

#ifndef MENUS_H
#define MENUS_H

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_principal ();

/**
* @brief Função que imprime o menu do comerciante com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_comerciante ();

/**
* @brief Função que imprime o menu de cadastramento de um produto com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_cadastramento ();

/**
* @brief Função que imprime o menu de alteracao de dados de uma bebida com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_bebida ();

/**
* @brief Função que imprime o menu de alteracao de dados de uma fruta com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_fruta ();

/**
* @brief Função que imprime o menu de alteracao de dados de um salgado com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_salgado ();

/**
* @brief Função que imprime o menu de alteracao de dados de um doce com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_doce ();

/**
* @brief Função que imprime o menu de alteracao de dados de um cd com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_cd ();

/**
* @brief Função que imprime o menu de alteracao de dados de um dvd com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_dvd ();

/**
* @brief Função que imprime o menu de alteracao de dados de um livro com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_alteracao_livro ();

/**
* @brief Função que imprime o menu de consulta do cliente com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_cliente ();

/**
* @brief Função que imprime o menu de consulta por tipo com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_tipo ();

/**
* @brief Função que imprime o menu de consulta do comerciante com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_consulta_comerciante ();

/**
* @brief Função que imprime o menu do carrinho de compras com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_carrinho ();

/**
* @brief Função que imprime o menu do cliente com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_cliente ();

#endif