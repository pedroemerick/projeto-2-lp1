/**
* @file     cadastro_produtos.h
* @brief    Declaracao dos prototipos das funcoes que executam o cadastramento de um produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef CADASTRO_PRODUTOS_H
#define CADASTRO_PRODUTOS_H

#include "produto.h"
#include "bebida.h"
#include "cd.h"
#include "doce.h"
#include "dvd.h"
#include "fruta.h"
#include "livro.h"
#include "salgado.h"
#include "fornecedor.h"
#include "menus.h"
#include "ll_dupla_ord.h"

/**
* @brief Função que recebe a opcao do usuario referente ao tipo de produto que deseja cadastrar
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void cadastramento_produto (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int &qtd_produtos);

/**
* @brief Função que faz a leitura dos dados para o cadastramento de um produto de acordo com o tipo desejado
* @param fornecedores Lista de fornecedores
* @param produtos Lista de produtos
* @param opcao Escolha do tipo de produto para cadastramento
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void cadastra_produto (Lista <Fornecedor> &fornecedores, Lista <Produto*> &produtos, int opcao, int &qtd_produtos);

#endif