/**
* @file   salgado.h
* @brief  Arquivo com os atributos e metodos da classe Salgado
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#ifndef SALGADO_H_
#define SALGADO_H_

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include "data.h"
#include "produto.h"
#include "produto_perecivel.h"

/**
 * @class   Salgado salgado.h
 * @brief   Classe que representa um Salgado
 * @details Os atributos de um salgado são a quantidade de sodio e a composicao (gluten e/ou lactose)
 */ 
class Salgado : public Produto, public Perecivel {
    private:
        float qtd_sodio;                    /**< Quantidade de sodio do salgado */
        string composicao;                  /**< Composicao do salgado (gluten e/ou lactose) */
        ostream& print (ostream &os) const {

            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Quantidade de sodio: " << qtd_sodio << " mg" << endl;
            os << "Composicao: " << composicao << endl;
            os << "Validade: " << validade << endl;

            return os;    
        }
    public:
        /** @brief Construtor padrao */
        Salgado ();

        /** @brief Construtor parametrizado */
        Salgado (string n, double c, string d, float p, int qe, float s, string cp, Fornecedor &f, Data v);

        /** @brief Retorna a quantidade de sodio do salgado */
        float getQtd_sodio ();

        /** @brief Modifica a quantidade de sodio do salgado */
        void setQtd_sodio (float qs);

        /** @brief Retorna a composicao do salgado */
        string getComposicao ();

        /** @brief Modifica a composicao do salgado */
        void setComposicao (string cp);

        /** @brief Retorna uma string com todos os dados do salgado */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraSalgado ();

        /** @brief Destrutor padrao */
        ~Salgado ();
};

#endif