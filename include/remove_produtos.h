/**
* @file     remove_produtos.h
* @brief    Declaracao dos prototipos da funcao que remove um produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    04/06/2017
*/

#ifndef REMOVE_PRODUTOS_H
#define REMOVE_PRODUTOS_H

#include "produto.h"
#include "ll_dupla_ord.h"

/**
* @brief Função que remove um produto do programa atraves do nome do produto
* @param produtos Lista de produtos
* @param qtd_produtos Quantidade de produtos cadastrados
*/
void remocao_produto (Lista <Produto*> &produtos, int &qtd_produtos);

#endif