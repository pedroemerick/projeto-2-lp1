/**
* @file     alteracao_produto.h
* @brief    Declaracao do prototipo da funcao que chama os metodos de alteracao de dados 
*           de acordo com o tipo do produto
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    05/06/2017
*/

#ifndef ALTERACAO_PRODUTO_H
#define ALTERACAO_PRODUTO_H

#include "produto.h"
#include "ll_dupla_ord.h"

/**
* @brief Função que recebe qual produto o usuario deseja alterar os dados 
*        e chama o respectivo metodo para a alteracao de dados
* @param produtos Lista com todos produtos cadastrados
*/
void alteracao_produto (Lista <Produto*> &produtos);

#endif