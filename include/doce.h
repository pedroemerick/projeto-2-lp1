/**
* @file   doce.h
* @brief  Arquivo com os atributos e metodos da classe Doce
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#ifndef DOCE_H_
#define DOCE_H_

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include "data.h"
#include "produto.h"
#include "produto_perecivel.h"

/**
 * @class   Doce doce.h
 * @brief   Classe que representa um doce
 * @details Os atributos de um doce são a quantidade de acucar e a composicao
 */ 
class Doce : public Produto, public Perecivel {
    private:
        float qtd_acucar;                               /**< Quantidade de acucar em miligramas do doce */
        string composicao;                              /**< Composicao do doce (gluten e/ou lactose) */
        ostream& print (ostream &os) const {
            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Quantidade de acucar: " << qtd_acucar << " mg" << endl;
            os << "Composicao: " << composicao << endl;
            os << "Validade: " << validade << endl;
            return os;    
        }
    public:
        /** @brief Construtor padrao */
        Doce ();

        /** @brief Construtor parametrizado */
        Doce (string n, double c, string d, float p, int qe, float q, string cp, Fornecedor &f, Data v);

        /** @brief Retorna a quantidade de acucar do doce */
        float getQtd_acucar ();

        /** @brief Modifica a quantidade de acucar do doce */  
        void setQtd_acucar (float qa);

        /** @brief Retorna a composicao do doce */
        string getComposicao ();

        /** @brief Modifica a composicao do doce */
        void setComposicao (string cp);

        /** @brief Retorna uma string com todos os dados do doce */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraDoce ();

        /** @brief Destrutor padrao */
        ~Doce ();
};

#endif