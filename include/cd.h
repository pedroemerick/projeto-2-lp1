/**
* @file   cd.h
* @brief  Arquivo com os atributos e metodos da classe CD
* @author Valmir Correa (valmircorrea96@outlook.com)
* @author Pedro Emerick (p.emerick@live.com)
* @since  30/05/2017
* @date   04/06/2017
*/

#include <string>
using std::string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

#include "produto.h"

#ifndef CD_H
#define CD_H

/**
 * @class   CD cd.h
 * @brief   Classe que representa um CD
 * @details Os atributos de um cd são o estilo do cd, artista e album
 */ 
class CD : public Produto {
    private:
        string estilo;                          /**< Estilo do cd */
        string artista;                         /**< Artista do cd */
        string album;                           /**< Album do cd */
        ostream& print (ostream &os) const {

            os << endl << "Tipo: " << tipo << endl;
            os << "Nome: " << nome << endl;
            os << "Código: " << codigo << endl;
            os << "Descrição: " << descricao << endl;
            os << "Preço: R$ " << preco << endl;
            os << "Quantidade em estoque: " << qtd_estoque << endl;
            os << "Estilo: " << estilo << endl;
            os << "Artista: " << artista << endl;
            os << "Album: " << album << endl;

            return os;    
        }
    public:
         /** @brief Construtor padrao */ 
        CD ();

        /** @brief Construtor parametrizado */
        CD (string n, double c, string d, float p, int qe, string e, string a, string ab, Fornecedor &f);

        /** @brief Retorna o estilo do cd */
        string getEstilo ();

        /** @brief Modifica estilo do cd */
        void setEstilo (string e);

        /** @brief Retorna o artista do cd */        
        string getArtista ();

        /** @brief Modifica o artista do cd */                
        void setArtista (string a);

        /** @brief Retorna qual o album do cd */        
        string getAlbum ();

        /** @brief Modifica o album do cd */        
        void setAlbum (string ab);

        /** @brief Retorna uma string com todos os dados do cd */
        string To_string ();

        /** @brief Modifica algum dado escolhido pelo usuario */
        void AlteraCD ();
        
        /** @brief Destrutor padrao */
        ~CD ();
};

#endif